/*
 * Decompiled with CFR 0_132.
 */
package cz.GravelCZLP.Breakpoint.sound;

import cz.GravelCZLP.Breakpoint.sound.BPSound;

public class BPSoundSet {
	private final BPSound[] sounds;

	public /* varargs */ BPSoundSet(BPSound... sounds) {
		this.sounds = sounds;
	}

	public BPSound[] getSounds() {
		return this.sounds;
	}
}
