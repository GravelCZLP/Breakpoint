/*
 * Decompiled with CFR 0_132.
 */
package cz.GravelCZLP.Breakpoint.managers;

import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.language.Translation;

public class ChatManager {
	public static String prefixAdmin;
	public static String prefixModerator;
	public static String prefixHelper;
	public static String prefixVIP;
	public static String prefixYT;
	public static String prefixSponsor;
	public static String prefixDeveloper;
	public static String tagPrefixVIP;
	public static String tagPrefixYT;
	public static String tagPrefixSponsor;
	public static String tagPrefixAdmin;
	public static String tagPrefixModerator;
	public static String tagPrefixHelper;
	public static String tagPrefixDeveloper;

	public static void loadStrings() {
		prefixAdmin = MessageType.CHAT_PREFIX_ADMIN.getTranslation().getValue(new Object[0]);
		prefixDeveloper = MessageType.CHAT_PREFIX_DEVELOPER.getTranslation().getValue(new Object[0]);
		prefixModerator = MessageType.CHAT_PREFIX_MODERATOR.getTranslation().getValue(new Object[0]);
		prefixHelper = MessageType.CHAT_PREFIX_HELPER.getTranslation().getValue(new Object[0]);
		prefixYT = MessageType.CHAT_PREFIX_YOUTUBE.getTranslation().getValue(new Object[0]);
		prefixSponsor = MessageType.CHAT_PREFIX_SPONSOR.getTranslation().getValue(new Object[0]);
		prefixVIP = MessageType.CHAT_PREFIX_VIP.getTranslation().getValue(new Object[0]);
		tagPrefixYT = MessageType.CHAT_TAGPREFIX_YOUTUBE.getTranslation().getValue(new Object[0]);
		tagPrefixSponsor = MessageType.CHAT_TAGPREFIX_SPONSOR.getTranslation().getValue(new Object[0]);
		tagPrefixVIP = MessageType.CHAT_TAGPREFIX_VIP.getTranslation().getValue(new Object[0]);
		tagPrefixAdmin = MessageType.CHAT_TAGPREFIX_ADMIN.getTranslation().getValue(new Object[0]);
		tagPrefixDeveloper = MessageType.CHAT_TAGPREFIX_DEVELOPER.getTranslation().getValue(new Object[0]);
		tagPrefixModerator = MessageType.CHAT_TAGPREFIX_MODERATOR.getTranslation().getValue(new Object[0]);
		tagPrefixHelper = MessageType.CHAT_TAGPREFIX_HELPER.getTranslation().getValue(new Object[0]);
	}
}
