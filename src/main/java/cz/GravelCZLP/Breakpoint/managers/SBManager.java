/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.OfflinePlayer
 *  org.bukkit.entity.Player
 *  org.bukkit.scoreboard.DisplaySlot
 *  org.bukkit.scoreboard.Objective
 *  org.bukkit.scoreboard.Score
 *  org.bukkit.scoreboard.Scoreboard
 */
package cz.GravelCZLP.Breakpoint.managers;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import cz.GravelCZLP.Breakpoint.game.Game;
import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;

public class SBManager {
	private final BPPlayer bpPlayer;
	private final Scoreboard sb;
	public Objective lobbyObj;
	public Objective voteObj;
	public Objective progressObj;

	public SBManager(BPPlayer bpPlayer) {
		this.bpPlayer = bpPlayer;
		this.sb = Bukkit.getScoreboardManager().getNewScoreboard();
		this.init();
		Player player = bpPlayer.getPlayer();
		player.setScoreboard(this.sb);
		this.updateSidebarObjective();
	}

	private void init() {
		this.initLobbyObj();
		this.initVoteObj();
		this.initProgressObj();
	}

	private void initLobbyObj() {
		this.lobbyObj = this.sb.registerNewObjective("LOBBY", "dummy");
		this.lobbyObj.setDisplayName(MessageType.SCOREBOARD_LOBBY_HEADER.getTranslation().getValue(new Object[0]));
	}

	private void initVoteObj() {
		this.voteObj = this.sb.registerNewObjective("VOTE", "dummy");
		this.voteObj.setDisplayName(MessageType.MAP_VOTING_HEADER.getTranslation().getValue(new Object[0]));
	}

	public void initProgressObj() {
		this.progressObj = this.sb.registerNewObjective("PROGRESS", "dummy");
		this.progressObj.setDisplayName("Undefined");
	}

	public void unregister() {
		for (Objective obj : this.sb.getObjectives()) {
			obj.unregister();
		}
		this.progressObj = null;
		this.voteObj = null;
		this.lobbyObj = null;
		this.sb.resetScores(this.getPlayer().getName());
	}

	public static void updateLobbyObjectives() {
		for (BPPlayer bpPlayer : BPPlayer.onlinePlayers) {
			if (!bpPlayer.isInLobby())
				continue;
			bpPlayer.getScoreboardManager().updateLobbyObjective();
		}
	}

	public void updateLobbyObjective() {
		for (Game game : GameManager.getGames()) {
			String name = game.getName();
			int players = game.getPlayers().size();
			Score score = this.lobbyObj.getScore(ChatColor.YELLOW + name);
			score.setScore(players);
		}
	}

	public void updateVoteOptions(Map<String, Integer> votes) {
		for (Map.Entry<String, Integer> entry : votes.entrySet()) {
			String name = entry.getKey();
			int voted = entry.getValue();
			if (name == null)
				continue;
			Score score = this.voteObj.getScore((Object) ChatColor.AQUA + name);
			score.setScore(voted);
		}
	}

	public void restartVoteObj() {
		this.voteObj.unregister();
		this.initVoteObj();
	}

	public void updateSidebarObjective() {
		Game game = this.bpPlayer.getGame();
		if (game != null) {
			if (!game.votingInProgress()) {
				this.progressObj.setDisplaySlot(DisplaySlot.SIDEBAR);
			} else {
				this.voteObj.setDisplaySlot(DisplaySlot.SIDEBAR);
			}
		} else {
			this.lobbyObj.setDisplaySlot(DisplaySlot.SIDEBAR);
		}
	}

	public static String formatTime(int timeLeft) {
		if (timeLeft <= 0) {
			return "0:00";
		}
		int seconds = timeLeft;
		int minutes = 0;
		while (seconds >= 60) {
			seconds -= 60;
			++minutes;
		}
		String sMinutes = minutes < 10 ? "0" + minutes : Integer.toString(minutes);
		String sSeconds = seconds < 10 ? "0" + seconds : Integer.toString(seconds);
		return sMinutes + ":" + sSeconds;
	}

	public Scoreboard getScoreboard() {
		return this.sb;
	}

	public BPPlayer getPlayer() {
		return this.bpPlayer;
	}

	public Objective getProgressObj() {
		if (this.progressObj == null) {
			this.initProgressObj();
		}
		return this.progressObj;
	}

	public void setProgressObj(Objective progressObj) {
		this.progressObj = progressObj;
	}
}
