/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.Material
 *  org.bukkit.entity.Player
 *  org.bukkit.map.MapCanvas
 *  org.bukkit.map.MapFont
 *  org.bukkit.map.MapView
 *  org.bukkit.map.MinecraftFont
 */
package cz.GravelCZLP.Breakpoint.managers.events.advent;

import cz.GravelCZLP.Breakpoint.Breakpoint;
import cz.GravelCZLP.Breakpoint.equipment.BPBlock;
import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.language.Translation;
import cz.GravelCZLP.Breakpoint.managers.events.advent.AdventGift;
import cz.GravelCZLP.Breakpoint.managers.events.advent.AdventManager;
import cz.GravelCZLP.Breakpoint.maps.BPMapPalette;
import cz.GravelCZLP.Breakpoint.maps.BPMapRenderer;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapFont;
import org.bukkit.map.MapView;
import org.bukkit.map.MinecraftFont;

public class AdventMapRenderer extends BPMapRenderer {
	public static final byte[] COLOR_BACKGROUND = new byte[] { BPMapPalette.getColor(11, 0),
			BPMapPalette.getColor(11, 1), BPMapPalette.getColor(11, 2), BPMapPalette.getColor(11, 1),
			BPMapPalette.getColor(11, 0) };
	public static final byte COLOR_HEADER = BPMapPalette.getColor(4, 2);
	public static final byte COLOR_HEADER_BACKGROUND = BPMapPalette.getColor(4, 1);
	public static final byte COLOR_DESCRIPTION = BPMapPalette.getColor(4, 1);
	public static final byte COLOR_DESCRIPTION_BACKGROUND = BPMapPalette.getColor(4, 0);
	public static final byte COLOR_TEXT = BPMapPalette.getColor(4, 2);
	public static final byte COLOR_TEXT_BACKGROUND = BPMapPalette.getColor(8, 2);
	public static final byte GLOW_COLOR = BPMapPalette.getColor(1, 2);
	public static final byte GLOW_COLOR_EARNED = BPMapPalette.getColor(1, 0);
	public static final byte HIDDEN_COLOR = BPMapPalette.getColor(13, 0);
	private final Byte[][][] numbers;
	private final Byte[][][] icons;
	private final int iconWidth = 6;
	private final int iconHeight;
	private final int iconStartX = 10;
	private final int iconStartY;
	private final int dayOfMonth;
	private final String header;
	private final String description;
	private final AdventManager advm;

	public AdventMapRenderer(AdventManager advm, int dayOfMonth) {
		this.advm = advm;
		this.getClass();
		this.iconHeight = (int) Math.ceil(24.0 / 6.0);
		this.iconStartY = 128 - (this.iconHeight * 18 - 1) - 10;
		ArrayList<AdventGift> gifts = advm.getGifts();
		this.icons = this.loadIcons(gifts);
		this.numbers = this.loadNumbers(gifts.size());
		this.header = MessageType.EVENT_ADVENT_MAP_HEADER.getTranslation().getValue(new Object[0]);
		this.description = MessageType.EVENT_ADVENT_MAP_DESCRIPTION.getTranslation().getValue(new Object[0]);
		this.dayOfMonth = dayOfMonth;
	}

	public void render(MapView view, MapCanvas canvas, Player player) {
		String playerName = player.getName();
		boolean earned = this.advm.getGift().hasEarned(playerName);
		AdventMapRenderer.drawRectangleFade(canvas, 0, 0, 128, 128, COLOR_BACKGROUND, 0);
		this.drawIcons(canvas);
		this.drawNumbers(canvas);
		this.drawHeader(canvas);
		this.drawDescription(canvas);
		this.drawGlow(canvas, earned);
	}

	public void drawGlow(MapCanvas canvas, boolean hasEarned) {
		int yy;
		int xx;
		int i = this.dayOfMonth - 1;
		this.getClass();
		int iconX = 10 + i % 6 * 18;
		int iconY = this.iconStartY + i / 6 * 18;
		byte color = !hasEarned ? GLOW_COLOR : GLOW_COLOR_EARNED;
		for (int x = 0; x < 2; ++x) {
			for (int y = 0; y < 18; ++y) {
				xx = iconX + x * 17 - 1;
				yy = iconY + y - 1;
				canvas.setPixel(xx, yy, color);
			}
		}
		for (int y = 0; y < 2; ++y) {
			for (int x = 0; x < 16; ++x) {
				xx = iconX + x;
				yy = iconY + y * 17 - 1;
				canvas.setPixel(xx, yy, color);
			}
		}
	}

	public void drawIcons(MapCanvas canvas) {
		for (int i = 0; i < this.icons.length; ++i) {
			int yy;
			int x;
			int xx;
			int y;
			this.getClass();
			int iconX = 10 + i % 6 * 18;
			int iconY = this.iconStartY + i / 6 * 18;
			if (this.icons[i] != null && i < this.dayOfMonth) {
				for (x = 0; x < this.icons[i].length; ++x) {
					for (y = 0; y < this.icons[i][x].length; ++y) {
						xx = iconX + x;
						yy = iconY + y;
						Byte color = this.icons[i][x][y];
						if (color == null)
							continue;
						canvas.setPixel(xx, yy, color.byteValue());
					}
				}
				continue;
			}
			for (x = 0; x < 16; ++x) {
				for (y = 0; y < 16; ++y) {
					xx = iconX + x;
					yy = iconY + y;
					canvas.setPixel(xx, yy, HIDDEN_COLOR);
				}
			}
		}
	}

	public void drawNumbers(MapCanvas canvas) {
		for (int i = 0; i < this.icons.length && i < this.dayOfMonth; ++i) {
			if (this.icons[i] == null)
				continue;
			this.getClass();
			int numberX = 10 + i % 6 * 18 + (16 - this.numbers[i].length) / 2;
			for (int x = 0; x < this.numbers[i].length; ++x) {
				int numberY = this.iconStartY + i / 6 * 18 + (16 - this.numbers[i][x].length) / 2;
				for (int y = 0; y < this.numbers[i][x].length; ++y) {
					int xx = numberX + x;
					int yy = numberY + y;
					Byte color = this.numbers[i][x][y];
					if (color == null)
						continue;
					canvas.setPixel(xx, yy, color.byteValue());
				}
			}
		}
	}

	public void drawHeader(MapCanvas canvas) {
		int width = AdventMapRenderer.getWidth(this.header) * 2;
		int height = MinecraftFont.Font.getHeight() * 2;
		int startX = (128 - width) / 2;
		int startY = (this.iconStartY - 1 - height) / 3;
		this.drawText(canvas, startX, startY + 1, 2, 2, (MapFont) MinecraftFont.Font, this.header,
				COLOR_HEADER_BACKGROUND);
		this.drawText(canvas, startX, startY, 2, 2, (MapFont) MinecraftFont.Font, this.header, COLOR_HEADER);
	}

	public void drawDescription(MapCanvas canvas) {
		int width = AdventMapRenderer.getWidth(this.description);
		int height = MinecraftFont.Font.getHeight();
		int startX = (128 - width) / 2;
		int startY = (this.iconStartY - 1 - height) / 4 * 3;
		this.drawText(canvas, startX, startY + 1, 1, 1, (MapFont) MinecraftFont.Font, this.description,
				COLOR_DESCRIPTION_BACKGROUND);
		this.drawText(canvas, startX, startY, 1, 1, (MapFont) MinecraftFont.Font, this.description, COLOR_DESCRIPTION);
	}

	public Byte[][][] loadIcons(ArrayList<AdventGift> gifts) {
		Byte[][][] icons = new Byte[24][][];
		for (int i = 0; i < icons.length; ++i) {
			AdventGift gift = gifts.get(i);
			BPBlock block = gift.getBlock();
			AdventMapRenderer.loadIcon(icons, block, i);
		}
		return icons;
	}

	public Byte[][][] loadNumbers(int giftsSize) {
		Byte[][][] numbers = new Byte[giftsSize][][];
		for (int i = 0; i < giftsSize; ++i) {
			String day = Integer.toString(i + 1);
			int width = AdventMapRenderer.getWidth(day) + 2;
			int height = MinecraftFont.Font.getHeight() + 2;
			Byte[][] number = new Byte[width][height];
			for (int x = 0; x < 3; ++x) {
				for (int y = 0; y < 3; ++y) {
					this.drawText(number, x, y, 1, 1, (MapFont) MinecraftFont.Font, day, COLOR_TEXT_BACKGROUND);
				}
			}
			this.drawText(number, 1, 1, 1, 1, (MapFont) MinecraftFont.Font, day, COLOR_TEXT);
			numbers[i] = number;
		}
		return numbers;
	}

	private static void loadIcon(Byte[][][] icons, BPBlock block, int index) {
		byte data;
		int id = block.getId();
		File file = new File("plugins/Breakpoint/images/textures/" + Material.getMaterial((int) id).name().toLowerCase()
				+ ((data = block.getData()) != 0 ? new StringBuilder().append("_").append(data).toString() : "")
				+ ".png");
		try {
			BufferedImage img = ImageIO.read(file);
			if (img == null) {
				return;
			}
			int width = img.getWidth();
			int height = img.getHeight();
			Byte[][] icon = new Byte[width < 16 ? width : 16][height < 16 ? height : 16];
			for (int x = 0; x < icon.length; ++x) {
				for (int y = 0; y < icon[x].length; ++y) {
					Color color = Color.decode(Integer.toString(img.getRGB(x, y)));
					icon[x][y] = BPMapPalette.matchColor(color);
				}
			}
			icons[index] = icon;
		} catch (Throwable e) {
			Breakpoint.warn("Error when loading gift icon: " + file.getName() + " (" + id + ":" + data + ")");
			e.printStackTrace();
		}
	}

	public AdventManager getAdventManager() {
		return this.advm;
	}
}
