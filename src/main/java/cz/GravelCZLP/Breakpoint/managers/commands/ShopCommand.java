/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.Location
 *  org.bukkit.block.Block
 *  org.bukkit.block.BlockFace
 *  org.bukkit.command.Command
 *  org.bukkit.command.CommandExecutor
 *  org.bukkit.command.CommandSender
 *  org.bukkit.entity.Player
 */
package cz.GravelCZLP.Breakpoint.managers.commands;

import cz.GravelCZLP.Breakpoint.equipment.BPSkull;
import cz.GravelCZLP.Breakpoint.managers.ShopManager;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ShopCommand implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (args.length == 0) {
			sender.sendMessage((Object) ChatColor.RED + "Nespravne Argumenty");
			sender.sendMessage((Object) ChatColor.GRAY
					+ "/shop buildShop armor [side] [#RGB] [cost,cost...] [time,time...] [name] (For armor)");
			sender.sendMessage((Object) ChatColor.GRAY
					+ "/shop buildShop skull [side] [skulltype (list for list of values)] [name] (For skull)");
			sender.sendMessage((Object) ChatColor.GRAY
					+ "/shop buildShop banner [side] [banner name from config(banners.yaml)] [name] (For banner) (SOON tm)");
			return false;
		}
		if (args[0].equalsIgnoreCase("buildShop")) {
			if (!(sender instanceof Player)) {
				return true;
			}
			Player player = (Player) sender;
			if (args[1].equalsIgnoreCase("armor")) {
				if (args.length >= 7) {
					int[] time;
					String name;
					String color;
					int[] cost;
					int facing;
					Location loc;
					try {
						String[] rawTime;
						loc = player.getLocation();
						facing = Integer.parseInt(args[2]);
						color = args[3];
						name = "";
						for (int i = 6; i < args.length; ++i) {
							name = name + args[i] + " ";
						}
						name = name.substring(0, name.length() - 1);
						String[] rawCost = args[4].split(",");
						if (rawCost.length != (rawTime = args[5].split(",")).length) {
							player.sendMessage((Object) ChatColor.RED + "Pocet casu se musi rovnat poctu cen.");
							return true;
						}
						cost = new int[rawCost.length];
						time = new int[rawTime.length];
						for (int i = 0; i < cost.length; ++i) {
							cost[i] = Integer.parseInt(rawCost[i]);
							time[i] = Integer.parseInt(rawTime[i]);
						}
					} catch (Exception e) {
						player.sendMessage((Object) ChatColor.RED + "Nespravne argumenty!");
						return true;
					}
					ShopManager.buildArmorShop(loc, facing, color, cost, time, name);
					player.sendMessage((Object) ChatColor.GREEN + "Obchod postaven!");
				} else {
					player.sendMessage((Object) ChatColor.RED + "Nespravne argumenty!");
					player.sendMessage((Object) ChatColor.GRAY
							+ "/bp buildShop [type (armor, skull)] [side] [#RGB] [cost,cost...] [time,time...] [name] (For armor)");
				}
			} else if (args[1].equalsIgnoreCase("skull")) {
				if (args.length >= 4) {
					if (args[3].equalsIgnoreCase("list")) {
						for (BPSkull.SkullType s : BPSkull.SkullType.values()) {
							String name = s.name() + ", Formatted name: " + s.getFormattedName();
							String alias = s.getAlias();
							int cost = s.getCost();
							boolean vip = s.isVip();
							sender.sendMessage(name + " " + alias + " " + cost + " " + vip);
						}
						return true;
					}
					Location loc = null;
					int facing = 0;
					String name = "";
					BPSkull.SkullType type = null;
					try {
						loc = player.getLocation();
						facing = Integer.parseInt(args[2]);
						for (int i = 4; i < args.length; ++i) {
							name = name + args[i] + " ";
						}
						name = ChatColor.translateAlternateColorCodes((char) '&', (String) name);
						if (!args[3].equalsIgnoreCase("chameleon")) {
							try {
								type = BPSkull.SkullType.valueOf(args[3].toUpperCase());
							} catch (IllegalArgumentException e) {
								player.sendMessage(args[3] + " is not a valid head type.");
							}
						}
						ShopManager.buildSkullShop(loc, facing, name, type,
								player.getLocation().getBlock().getFace(player.getLocation().getBlock()));
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					player.sendMessage((Object) ChatColor.RED + "Nespravne Argumenty");
					player.sendMessage((Object) ChatColor.GRAY
							+ "/bp buildShop [type (skull, armor] [side] [skulltype (list for list of values)] [name] (For skull)");
				}
			} else if (args[1].equalsIgnoreCase("banner")) {
				if (args.length >= 4) {
					// empty if block
				}
			} else {
				sender.sendMessage((Object) ChatColor.RED + "/bp buildShop [type (skull, armor)] args....");
			}
		} else {
			sender.sendMessage((Object) ChatColor.RED + "Nespravne Argumenty");
			sender.sendMessage((Object) ChatColor.GRAY
					+ "/shop buildShop [type (armor, skull)] [side] [#RGB] [cost,cost...] [time,time...] [name] (For armor)");
			sender.sendMessage((Object) ChatColor.GRAY
					+ "/shop buildShop [type (skull, armor] [side] [skulltype (list for list of values)] [name] (For skull)");
			sender.sendMessage((Object) ChatColor.GRAY
					+ "/shop buildShop banner [side [banner name from config(banners.yaml)] [name] (For banner) (SOON tm)");
		}
		return false;
	}
}
