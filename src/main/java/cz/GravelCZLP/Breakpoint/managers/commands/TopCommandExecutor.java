/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.command.Command
 *  org.bukkit.command.CommandExecutor
 *  org.bukkit.command.CommandSender
 */
package cz.GravelCZLP.Breakpoint.managers.commands;

import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.language.Translation;
import cz.GravelCZLP.Breakpoint.managers.StatisticsManager;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class TopCommandExecutor implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (args.length <= 0) {
			StatisticsManager.listTopPlayers(sender, 10, 1);
		} else {
			int page;
			try {
				page = Integer.parseInt(args[0]);
			} catch (NumberFormatException e) {
				sender.sendMessage(MessageType.COMMAND_TOP_EXE_INCORRECTPAGE.getTranslation().getValue(new Object[0]));
				return true;
			}
			if (page > 0) {
				StatisticsManager.listTopPlayers(sender, 10, page);
			} else {
				sender.sendMessage(MessageType.COMMAND_TOP_EXE_NEGATIVEORZERO.getTranslation().getValue(new Object[0]));
			}
		}
		return true;
	}
}
