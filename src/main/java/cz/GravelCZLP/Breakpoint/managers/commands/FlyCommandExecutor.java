/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.command.Command
 *  org.bukkit.command.CommandExecutor
 *  org.bukkit.command.CommandSender
 *  org.bukkit.entity.Player
 */
package cz.GravelCZLP.Breakpoint.managers.commands;

import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.language.Translation;
import cz.GravelCZLP.Breakpoint.managers.VIPManager;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class FlyCommandExecutor implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			return true;
		}
		Player player = (Player) sender;
		BPPlayer bpPlayer = BPPlayer.get(player);
		boolean b = player.hasPermission("Breakpoint.fly");
		if (!b) {
			player.sendMessage(MessageType.COMMAND_FLY_VIPPLUSSONLY.getTranslation().getValue(new Object[0]));
			return true;
		}
		if (!bpPlayer.isInLobby()) {
			player.sendMessage(MessageType.COMMAND_FLY_NOTLOBBY.getTranslation().getValue(new Object[0]));
			return true;
		}
		if (VIPManager.isFarFromSpawnToUseFly(player)) {
			player.sendMessage(MessageType.COMMAND_FLY_TOOFAR.getTranslation().getValue(new Object[0]));
			return true;
		}
		boolean value = !player.getAllowFlight();
		player.setAllowFlight(value);
		if (value) {
			player.sendMessage(MessageType.COMMAND_FLY_ENABLED.getTranslation().getValue(new Object[0]));
		} else {
			player.sendMessage(MessageType.COMMAND_FLY_DISABLED.getTranslation().getValue(new Object[0]));
		}
		return true;
	}
}
