/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.GameMode
 *  org.bukkit.command.Command
 *  org.bukkit.command.CommandExecutor
 *  org.bukkit.command.CommandSender
 *  org.bukkit.entity.Player
 */
package cz.GravelCZLP.Breakpoint.managers.commands;

import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.language.Translation;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GMCommandExecutor implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			return true;
		}
		Player player = (Player) sender;
		BPPlayer bpPlayer = BPPlayer.get(player);
		if (!player.hasPermission("Breakpoint.creative")) {
			player.sendMessage(MessageType.OTHER_NOPERMISSION.getTranslation().getValue(new Object[0]));
			return true;
		}
		if (!bpPlayer.isInLobby()) {
			player.sendMessage((Object) ChatColor.RED + "Available in the lobby only.");
			return true;
		}
		GameMode gm = player.getGameMode();
		GameMode newGM = gm == GameMode.ADVENTURE ? GameMode.CREATIVE : GameMode.ADVENTURE;
		player.setGameMode(newGM);
		player.sendMessage((Object) ChatColor.AQUA + "GameMode changed to " + newGM.name());
		return true;
	}
}
