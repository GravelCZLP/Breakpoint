/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.command.Command
 *  org.bukkit.command.CommandExecutor
 *  org.bukkit.command.CommandSender
 *  org.bukkit.entity.Player
 */
package cz.GravelCZLP.Breakpoint.managers.commands;

import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.language.Translation;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HelpOPCommandExecutor implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage("Players only.");
			return false;
		}
		Player player = (Player) sender;
		if (args.length <= 0) {
			player.sendMessage(MessageType.COMMAND_HELPOP_USAGE.getTranslation().getValue(label));
			return false;
		}
		String question = args[0];
		for (int i = 1; i < args.length; ++i) {
			question = question + " " + args[i];
		}
		boolean success = HelpOPCommandExecutor.askHelpers(player, question);
		if (success) {
			player.sendMessage(MessageType.COMMAND_HELPOP_SUCCESS.getTranslation().getValue(new Object[0]));
			return true;
		}
		player.sendMessage(MessageType.COMMAND_HELPOP_FAILURE.getTranslation().getValue(new Object[0]));
		return false;
	}

	private static boolean askHelpers(Player sender, String question) {
		int count = 0;
		for (Player player : Bukkit.getOnlinePlayers()) {
			BPPlayer bpPlayer = BPPlayer.get(player);
			if (!bpPlayer.getPlayer().hasPermission("Breakpoint.helper"))
				continue;
			player.sendMessage((Object) ChatColor.LIGHT_PURPLE + "" + (Object) ChatColor.BOLD + sender.getName() + ": "
					+ question);
			++count;
		}
		return count > 0;
	}
}
