/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.command.Command
 *  org.bukkit.command.CommandExecutor
 *  org.bukkit.command.CommandSender
 */
package cz.GravelCZLP.Breakpoint.managers.commands;

import cz.GravelCZLP.Breakpoint.Breakpoint;
import cz.GravelCZLP.Breakpoint.Configuration;
import cz.GravelCZLP.Breakpoint.game.Game;
import cz.GravelCZLP.Breakpoint.game.cw.CWGame;
import cz.GravelCZLP.Breakpoint.game.cw.CWScheduler;
import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.language.Translation;
import cz.GravelCZLP.Breakpoint.managers.GameManager;
import cz.GravelCZLP.Breakpoint.players.clans.Clan;
import cz.GravelCZLP.Breakpoint.players.clans.ClanChallenge;
import java.util.Calendar;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CWCommandExecutor implements CommandExecutor {
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		String string;
		Configuration config = Breakpoint.getBreakpointConfig();
		Game rawGame = GameManager.getGame(config.getCWChallengeGame());
		if (rawGame == null || !(rawGame instanceof CWGame)) {
			sender.sendMessage(MessageType.COMMAND_CW_EXE_NOGAME.getTranslation().getValue(new Object[0]));
			return false;
		}
		CWGame game = (CWGame) rawGame;
		CWScheduler sc = game.getScheduler();
		StringBuilder sb = new StringBuilder();
		Calendar calendar = Calendar.getInstance();
		int dayOfYear = calendar.get(6);
		int dayOfWeek = calendar.get(7);
		ClanChallenge today = sc.getDay(dayOfYear);
		if (today == null) {
			string = MessageType.COMMAND_CW_DAYNOTTAKEN.getTranslation().getValue(new Object[0]);
		} else {
			Object[] arrobject = new Object[3];
			arrobject[0] = today.getChallengingClan() != null ? today.getChallengingClan().getColoredName() : "?";
			arrobject[1] = today.getChallengedClan() != null ? today.getChallengedClan().getColoredName() : "?";
			arrobject[2] = today.getMaximumPlayers();
			string = MessageType.COMMAND_CW_FORMAT_DAY.getTranslation().getValue(arrobject);
		}
		sb.append(ChatColor.YELLOW.toString()).append(ChatColor.BOLD.toString())
				.append(MessageType.CALENDAR_TODAY.getTranslation().getValue(new Object[0])).append(": ")
				.append(string);
		for (int i = 1; i <= 7; ++i) {
			String string2;
			ClanChallenge day = sc.getDay(++dayOfYear);
			if (++dayOfWeek > 7) {
				dayOfWeek -= 7;
			}
			if (day == null) {
				string2 = MessageType.COMMAND_CW_DAYNOTTAKEN.getTranslation().getValue(new Object[0]);
			} else {
				Object[] arrobject = new Object[3];
				arrobject[0] = day.getChallengingClan() != null ? day.getChallengingClan().getColoredName() : "?";
				arrobject[1] = day.getChallengedClan() != null ? day.getChallengedClan().getColoredName() : "?";
				arrobject[2] = day.getMaximumPlayers();
				string2 = MessageType.COMMAND_CW_FORMAT_DAY.getTranslation().getValue(arrobject);
			}
			sb.append('\n').append(ChatColor.YELLOW.toString()).append(CWScheduler.getDayName(dayOfWeek)).append(": ")
					.append(string2);
		}
		sender.sendMessage(MessageType.COMMAND_CW_FORMAT_MESSAGE.getTranslation().getValue(sb.toString()));
		return false;
	}
}
