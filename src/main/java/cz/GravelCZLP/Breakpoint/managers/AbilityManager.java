/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.Effect
 *  org.bukkit.Location
 *  org.bukkit.Material
 *  org.bukkit.Particle
 *  org.bukkit.Sound
 *  org.bukkit.World
 *  org.bukkit.block.Block
 *  org.bukkit.entity.Arrow
 *  org.bukkit.entity.Entity
 *  org.bukkit.entity.EntityType
 *  org.bukkit.entity.Fireball
 *  org.bukkit.entity.Player
 *  org.bukkit.entity.SmallFireball
 *  org.bukkit.potion.PotionEffect
 *  org.bukkit.potion.PotionEffectType
 *  org.bukkit.projectiles.ProjectileSource
 *  org.bukkit.util.Vector
 */
package cz.GravelCZLP.Breakpoint.managers;

import cz.GravelCZLP.Breakpoint.Breakpoint;
import java.util.Random;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.entity.SmallFireball;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.util.Vector;

public class AbilityManager {
	Breakpoint plugin;

	public AbilityManager(Breakpoint p) {
		this.plugin = p;
	}

	public static Fireball launchFireball(Player owner, Location loc, Vector vec) {
		World world = loc.getWorld();
		Fireball fb = (Fireball) world.spawnEntity(loc.add(vec), EntityType.FIREBALL);
		fb.setShooter((ProjectileSource) owner);
		fb.setVelocity(vec.multiply(2.0));
		world.playSound(loc, Sound.ENTITY_WITHER_SHOOT, 1.0f, 1.0f);
		return fb;
	}

	public static Fireball launchSmallFireball(Player owner, Location loc, Vector vec) {
		World world = loc.getWorld();
		Fireball fb = (Fireball) world.spawnEntity(loc.add(vec), EntityType.SMALL_FIREBALL);
		fb.setShooter((ProjectileSource) owner);
		fb.setVelocity(vec.multiply(2.0));
		world.playSound(loc, Sound.ENTITY_GHAST_SHOOT, 1.0f, 2.0f);
		return fb;
	}

	public static void fireballHit(Fireball fb) {
		Location loc = fb.getLocation();
		AbilityManager.showCracks(loc, 2);
		AbilityManager.smoke(loc, 16);
	}

	public static void smallFireballHit(SmallFireball fb) {
		Location loc = fb.getLocation();
		World world = loc.getWorld();
		AbilityManager.showCracks(loc, 1);
		world.playEffect(loc, Effect.MOBSPAWNER_FLAMES, 0);
	}

	public static Vector launchPlayer(Player player) {
		Vector vec = AbilityManager.getDirection(player);
		vec.setY(vec.getY() * 1.3);
		Location loc = player.getLocation();
		World world = loc.getWorld();
		player.setVelocity(vec);
		world.playSound(loc, Sound.BLOCK_LAVA_POP, 0.5f, new Random().nextFloat());
		return vec;
	}

	public static Vector getDirection(Player player) {
		Location loc = player.getLocation();
		Vector vec = loc.getDirection();
		return vec;
	}

	public static boolean isHeadshot(Location shooterLocation, Location victimLocation, Arrow arrow) {
		double victimY;
		Location arrowLocation = arrow.getLocation();
		double arrowY = arrowLocation.getY() + arrow.getVelocity().getY() / 2.0;
		boolean headshot = arrowY - (victimY = victimLocation.getY()) > 1.35
				&& victimLocation.distance(shooterLocation) > 8.0;
		return headshot;
	}

	public static void playHeadshotEffect(Player player) {
		Location loc = player.getLocation();
		World world = loc.getWorld();
		player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20, 1));
		world.playEffect(loc, Effect.ZOMBIE_CHEW_IRON_DOOR, 0);
	}

	public static void showCracks(Location loc, int radius) {
		World world = loc.getWorld();
		for (int x = -radius; x <= radius; ++x) {
			for (int y = -radius; y <= radius; ++y) {
				for (int z = -radius; z <= radius; ++z) {
					Location curLoc = loc.clone().add((double) x, (double) y, (double) z);
					if (curLoc.distance(loc) > (double) radius)
						continue;
					Block block = world.getBlockAt(curLoc);
					world.playEffect(curLoc, Effect.STEP_SOUND, (Object) block.getType());
				}
			}
		}
	}

	public static void showJumpEffect(Location loc, int radius) {
		World w = loc.getWorld();
		for (int x = -radius; x <= radius; ++x) {
			for (int z = -radius; z < radius; ++z) {
				Location curLoc = loc.clone().add((double) x, 0.0, (double) z);
				if (curLoc.distance(loc) > (double) radius)
					continue;
				w.spawnParticle(Particle.REDSTONE, curLoc, 5, 0.5, 0.0, 0.5);
			}
		}
	}

	public static void smoke(Location loc, int amount) {
		World world = loc.getWorld();
		Random rnd = new Random();
		for (int i = 0; i < amount; ++i) {
			world.playEffect(loc, Effect.SMOKE, rnd.nextInt(16));
		}
	}
}
