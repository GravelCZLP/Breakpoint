/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.entity.Player
 *  org.bukkit.plugin.Plugin
 */
package cz.GravelCZLP.Breakpoint.managers;

import cz.GravelCZLP.Breakpoint.Breakpoint;
import cz.GravelCZLP.Breakpoint.Configuration;
import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.language.Translation;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class LobbyInfoManager {
	public static void startLoop() {
		Bukkit.getScheduler().scheduleSyncRepeatingTask((Plugin) Breakpoint.getInstance(), new Runnable() {

			@Override
			public void run() {
				LobbyInfoManager.sendMessage();
			}
		}, 2400L, 2400L);
	}

	private static void sendMessage() {
		Configuration config = Breakpoint.getBreakpointConfig();
		List<String> messages = config.getLobbyMessages();
		if (messages.isEmpty()) {
			return;
		}
		String message = MessageType.CHAT_BREAKPOINT.getTranslation().getValue(new Object[0]) + " "
				+ messages.get(new Random().nextInt(messages.size()));
		for (BPPlayer bpPlayer : BPPlayer.onlinePlayers) {
			if (BPPlayer.onlinePlayers.isEmpty() || BPPlayer.onlinePlayers == null) {
				return;
			}
			if (!bpPlayer.isInLobby())
				continue;
			Player player = bpPlayer.getPlayer();
			player.sendMessage(message);
		}
	}

}
