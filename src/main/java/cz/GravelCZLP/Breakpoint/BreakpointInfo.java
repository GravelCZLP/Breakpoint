/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.Server
 *  org.bukkit.entity.Player
 */
package cz.GravelCZLP.Breakpoint;

import org.bukkit.entity.Player;

import cz.GravelCZLP.Breakpoint.game.Game;
import cz.GravelCZLP.Breakpoint.game.GameType;
import cz.GravelCZLP.Breakpoint.game.ctf.CTFGame;
import cz.GravelCZLP.Breakpoint.game.ctf.Team;
import cz.GravelCZLP.Breakpoint.game.dm.DMGame;
import cz.GravelCZLP.Breakpoint.managers.GameManager;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;

public class BreakpointInfo {
	private int pil;
	private int pig;
	private int rc;
	private int bc;
	private String bestPlayerDm;
	private String Cwg;
	private int pictf;
	private int pidm;

	public static BreakpointInfo getActualInfo() {
		CTFGame ctfGame = null;
		DMGame dmGame = null;
		Breakpoint bp = Breakpoint.getInstance();
		for (Game g : GameManager.getGames()) {
			if (g.getType() == GameType.CTF) {
				ctfGame = (CTFGame) g;
			}
			if (g.getType() != GameType.DM)
				continue;
			dmGame = (DMGame) g;
		}
		int playersInLobby = 0;
		int playersInGame = 0;
		int playersInCTF = 0;
		int playersInDM = 0;
		for (Player p : bp.getServer().getOnlinePlayers()) {
			BPPlayer bpPlayer = BPPlayer.get(p);
			if (!bpPlayer.isInGame()) {
				++playersInLobby;
				continue;
			}
			++playersInGame;
			if (bpPlayer.getGame().getType() == GameType.CTF) {
				++playersInCTF;
				continue;
			}
			if (bpPlayer.getGame().getType() != GameType.DM)
				continue;
			++playersInDM;
		}
		int blueId = Team.getId(Team.BLUE);
		int redId = Team.getId(Team.RED);
		int pointsBlue = ctfGame.getFlagManager().getScore()[blueId];
		int pointsRed = ctfGame.getFlagManager().getScore()[redId];
		String CWChallangeGame = Breakpoint.getBreakpointConfig().getCWChallengeGame();
		String bestPlayerInDM = dmGame.getCurrentBestPlayer();
		return new BreakpointInfo(playersInLobby, playersInGame, pointsRed, pointsBlue, bestPlayerInDM, CWChallangeGame,
				playersInCTF, playersInDM);
	}

	public BreakpointInfo(int playersInLobby, int playersInGame, int redCrystals, int blueCrystals,
			String bestPlayerInDM, String CWGame2, int playersInCTF, int playersInDM) {
		this.pil = playersInLobby;
		this.pig = playersInGame;
		this.rc = redCrystals;
		this.bc = blueCrystals;
		this.bestPlayerDm = bestPlayerInDM;
		this.Cwg = CWGame2;
		this.pictf = playersInCTF;
		this.pidm = playersInDM;
	}

	public int getPlayerIsLobby() {
		return this.pil;
	}

	public int getPlayerInGame() {
		return this.pig;
	}

	public int getRedCrystals() {
		return this.rc;
	}

	public int getBlueCrystals() {
		return this.bc;
	}

	public String getBestPlayerInDM() {
		return this.bestPlayerDm;
	}

	public String getCWGame() {
		return this.Cwg;
	}

	public int getPlayersInCTF() {
		return this.pictf;
	}

	public int getPlayersInDM() {
		return this.pidm;
	}
}
