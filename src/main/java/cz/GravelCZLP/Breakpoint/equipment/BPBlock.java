/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 */
package cz.GravelCZLP.Breakpoint.equipment;

import cz.GravelCZLP.Breakpoint.equipment.BPEquipment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class BPBlock extends BPEquipment {
	private int id;
	private byte data;

	public BPBlock(String name, int minutesLeft, int id, byte data) {
		super(name, minutesLeft);
		this.id = id;
		this.data = data;
	}

	@Override
	protected ItemStack getItemStackRaw() {
		String name = this.getName();
		ItemStack is = new ItemStack(this.id, 1, (short) this.data);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(name);
		is.setItemMeta(im);
		return is;
	}

	@Override
	protected String serializeRaw() {
		return this.getName() + "," + this.getMinutesLeft() + "," + this.id + "," + this.data;
	}

	@Override
	public String getEquipmentLabel() {
		return "block";
	}

	@Override
	public BPBlock clone() {
		return new BPBlock(this.getName(), this.getMinutesLeft(), this.getId(), this.getData());
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte getData() {
		return this.data;
	}

	public void setData(byte data) {
		this.data = data;
	}
}
