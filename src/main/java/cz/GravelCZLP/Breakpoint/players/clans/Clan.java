/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.configuration.file.YamlConfiguration
 *  org.bukkit.entity.Player
 */
package cz.GravelCZLP.Breakpoint.players.clans;

import cz.GravelCZLP.Breakpoint.Breakpoint;
import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.language.Translation;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import cz.GravelCZLP.Breakpoint.players.clans.ClanChallenge;
import cz.GravelCZLP.Breakpoint.statistics.CWMatchResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import me.limeth.storageAPI.Storage;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class Clan {
	public static final char[] allowedChars = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b',
			'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
			'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
			'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
	public static final int maxLength = 10;
	public static final int minLength = 3;
	public static final List<Clan> clans = new LinkedList<Clan>();
	private final String leader;
	private final List<String> moderators;
	private final List<String> members;
	private final List<String> invited;
	private final LinkedList<ClanChallenge> pendingChallenges;
	private final LinkedList<CWMatchResult> matchResults;
	private String name;
	private String coloredName;
	private String coloredNameRaw;

	public static Clan create(String name, String leader) {
		Clan bpClan = new Clan(name, leader);
		clans.add(bpClan);
		return bpClan;
	}

	public static void remove(String name) {
		String colorLess = ChatColor.stripColor((String) name);
		for (Clan bpClan : clans) {
			if (!bpClan.getName().equalsIgnoreCase(colorLess))
				continue;
			clans.remove(bpClan);
			break;
		}
	}

	public static Clan load(File file) {
		try {
			YamlConfiguration yml = YamlConfiguration.loadConfiguration((File) file);
			String clanNameColored = yml.getString("name");
			String leader = yml.getString("leader");
			List<String> moderators = yml.getStringList("moderators");
			List<String> members = yml.getStringList("members");
			List<String> invited = yml.getStringList("invited");
			List<String> rawMatchResults = yml.getStringList("matchResults");
			LinkedList<CWMatchResult> matchResults = new LinkedList<CWMatchResult>();
			if (rawMatchResults != null) {
				for (String rawMatchResult : rawMatchResults) {
					try {
						matchResults.add(CWMatchResult.unserialize(rawMatchResult));
					} catch (Exception e) {
						Breakpoint.warn(
								"Error when unserializing rawMatchResult (" + clanNameColored + "): " + e.getMessage());
					}
				}
			}
			return new Clan(clanNameColored, leader, moderators, members, invited, matchResults);
		} catch (Exception e) {
			Breakpoint.warn("Clan '" + file.getPath() + "' was not loaded properly.");
			e.printStackTrace();
			return null;
		}
	}

	public static Clan newLoad(Storage storage) throws Exception {
		try {
			String clanNameColored = storage.get(String.class, "name", null);
			String leader = storage.get(String.class, "leader");
			List<String> moderators = storage.getList("moderators", String.class);
			List<String> members = storage.getList("members", String.class);
			List<String> invited = storage.getList("invited", String.class);
			List<String> rawMatchResults = storage.getList("matchResults", String.class);
			LinkedList<CWMatchResult> matchResults = new LinkedList<CWMatchResult>();
			if (rawMatchResults != null) {
				for (String rawMatchResult : rawMatchResults) {
					try {
						matchResults.add(CWMatchResult.unserialize(rawMatchResult));
					} catch (Exception e) {
						Breakpoint.warn(
								"Error when unserializing rawMatchResult (" + clanNameColored + "): " + e.getMessage());
					}
				}
			}
			return new Clan(clanNameColored, leader, moderators, members, invited, matchResults);
		} catch (Exception e) {
			Breakpoint.warn("Clan '" + storage.getName() + "' was not loaded properly.");
			e.printStackTrace();
			return null;
		}
	}

	public static Clan load(String clanName) {
		File file = new File("plugins/Breakpoint/clans/" + clanName + ".yml");
		return Clan.load(file);
	}

	public static Clan newLoad(String clanName) throws Exception {
		return Clan.newLoad(new Storage(clanName));
	}

	public static void loadClans() {
		File folder = new File("plugins/Breakpoint/clans");
		File[] clanFiles = folder.listFiles();
		if (clanFiles != null) {
			for (File clanFile : clanFiles) {
				if (!clanFile.getPath().endsWith(".yml"))
					continue;
				Clan bpClan = Clan.load(clanFile);
				clans.add(bpClan);
			}
			for (Clan clan : clans) {
				clan.loadPendingChallenges();
			}
		}
	}

	public static void saveClans() throws IOException {
		Clan.clearClansFolder();
		for (Clan bpClan : clans) {
			bpClan.save();
		}
	}

	private static void clearClansFolder() {
		File folder = new File("plugins/Breakpoint/clans");
		File[] clanFiles = folder.listFiles();
		if (clanFiles != null) {
			for (File clanFile : clanFiles) {
				clanFile.delete();
			}
		}
	}

	public static boolean isCorrectLength(String name) {
		String colorLess = ChatColor.stripColor((String) name);
		int length = colorLess.length();
		return length >= 3 && length <= 10;
	}

	public static boolean hasCorrectCharacters(String name) {
		String colorLess = ChatColor.stripColor((String) name);
		for (int i = 0; i < colorLess.length(); ++i) {
			char c = colorLess.charAt(i);
			boolean isCorrect = false;
			for (char curC : allowedChars) {
				if (curC != c)
					continue;
				isCorrect = true;
				break;
			}
			if (isCorrect)
				continue;
			return false;
		}
		return true;
	}

	public static boolean exists(String name) {
		String colorLess = ChatColor.stripColor((String) name);
		for (Clan bpClan : clans) {
			if (!bpClan.getName().equalsIgnoreCase(colorLess))
				continue;
			return true;
		}
		return false;
	}

	public static Clan get(String name) {
		String colorLess = ChatColor.stripColor((String) name);
		for (Clan bpClan : clans) {
			if (!bpClan.getName().equalsIgnoreCase(colorLess))
				continue;
			return bpClan;
		}
		return null;
	}

	public static Clan getByLeader(String leader) {
		for (Clan bpClan : clans) {
			if (!bpClan.isLeader(leader))
				continue;
			return bpClan;
		}
		return null;
	}

	public static Clan getByMember(String member) {
		for (Clan bpClan : clans) {
			if (!bpClan.isMember(member))
				continue;
			return bpClan;
		}
		return null;
	}

	public static Clan getByModerator(String moderator) {
		for (Clan bpClan : clans) {
			if (!bpClan.isModerator(moderator))
				continue;
			return bpClan;
		}
		return null;
	}

	public static Clan getByPlayer(String player) {
		for (Clan bpClan : clans) {
			if (!bpClan.isLeader(player) && !bpClan.isMember(player) && !bpClan.isModerator(player))
				continue;
			return bpClan;
		}
		return null;
	}

	public static String getChatPrefix(BPPlayer bpPlayer) {
		Clan clan = bpPlayer.getClan();
		if (clan != null) {
			return (Object) ChatColor.GRAY + clan.getColoredName() + " ";
		}
		return "";
	}

	public static String getColored(String raw) {
		raw = ChatColor.translateAlternateColorCodes((char) '&', (String) raw);
		return raw;
	}

	public Clan(String coloredNameRaw, String leader, List<String> moderators, List<String> members,
			List<String> invited, LinkedList<ClanChallenge> pendingChallenges, LinkedList<CWMatchResult> matchResults) {
		if (coloredNameRaw == null) {
			throw new IllegalArgumentException("coloredNameRaw == null");
		}
		if (leader == null) {
			throw new IllegalArgumentException("leader == null");
		}
		this.setColoredNameRaw(coloredNameRaw);
		this.leader = leader;
		this.moderators = moderators != null ? moderators : new LinkedList();
		this.members = members != null ? members : new LinkedList();
		this.invited = invited != null ? invited : new LinkedList();
		this.pendingChallenges = pendingChallenges != null ? pendingChallenges : new LinkedList();
		this.matchResults = matchResults != null ? matchResults : new LinkedList();
	}

	public Clan(String coloredNameRaw, String leader, List<String> moderators, List<String> members,
			List<String> invited, LinkedList<CWMatchResult> matchResults) {
		this(coloredNameRaw, leader, moderators, members, invited, null, matchResults);
	}

	public Clan(String coloredNameRaw, String leader) {
		this(coloredNameRaw, leader, null, null, null, null);
	}

	public void loadPendingChallenges() {
		YamlConfiguration yml = YamlConfiguration.loadConfiguration((File) this.getFile());
		List<String> rawPendingChallenges = yml.getStringList("pendingChallenges");
		if (rawPendingChallenges != null) {
			for (String rawChallenge : rawPendingChallenges) {
				try {
					this.getPendingChallenges().add(ClanChallenge.unserialize(rawChallenge));
				} catch (Exception e) {
					Breakpoint.warn("Error when unserializing rawChallenge (" + this.name + "): " + e.getMessage());
				}
			}
		}
	}

	public void save() throws IOException {
		String clanNameColored = this.getColoredNameRaw();
		String clanName = this.getName();
		File dataFile = new File("plugins/Breakpoint/clans/" + clanName + ".yml");
		YamlConfiguration yamlData = YamlConfiguration.loadConfiguration((File) dataFile);
		LinkedList<String> rawPendingChallenges = new LinkedList<String>();
		LinkedList<String> rawMatchResults = new LinkedList<String>();
		for (ClanChallenge challenge : this.pendingChallenges) {
			if (!challenge.isWaiting())
				continue;
			rawPendingChallenges.add(challenge.serialize());
		}
		for (CWMatchResult result : this.matchResults) {
			rawMatchResults.add(result.serialize());
		}
		yamlData.set("name", (Object) clanNameColored);
		yamlData.set("leader", (Object) this.leader);
		yamlData.set("moderators", this.moderators);
		yamlData.set("members", this.members);
		yamlData.set("invited", this.invited);
		yamlData.set("pendingChallenges", rawPendingChallenges);
		yamlData.set("matchResults", rawMatchResults);
		yamlData.save(dataFile);
	}

	public void join(BPPlayer bpPlayer) {
		Player player = bpPlayer.getPlayer();
		String playerName = player.getName();
		String clanNameColored = this.getColoredName();
		this.informPlayers(MessageType.CLAN_OTHERJOIN.getTranslation().getValue(playerName));
		this.addMember(playerName);
		this.removeInvited(playerName);
		bpPlayer.setClan(this);
		player.sendMessage(MessageType.CLAN_JOIN.getTranslation().getValue(clanNameColored));
	}

	public void kick(String targetName) {
		Player target = Bukkit.getPlayerExact((String) targetName);
		String clanNameColored = this.getColoredName();
		this.removeModerator(targetName);
		this.removeMember(targetName);
		this.informPlayers(MessageType.CLAN_OTHERKICK.getTranslation().getValue(targetName));
		if (target != null) {
			BPPlayer bpTarget = BPPlayer.get(target);
			bpTarget.setClan(null);
			target.sendMessage(MessageType.CLAN_KICK.getTranslation().getValue(clanNameColored));
		}
	}

	public void leave(Player player) {
		BPPlayer bpPlayer = BPPlayer.get(player);
		String playerName = player.getName();
		String clanNameColored = this.getColoredName();
		this.removeModerator(playerName);
		this.removeMember(playerName);
		bpPlayer.setClan(null);
		this.informPlayers(MessageType.CLAN_OTHERLEAVE.getTranslation().getValue(playerName));
		player.sendMessage(MessageType.CLAN_LEAVE.getTranslation().getValue(clanNameColored));
	}

	public void breakup(Player byWho) {
		clans.remove(this);
		for (String memberName : this.getMemberNames()) {
			Player member = Bukkit.getPlayerExact((String) memberName);
			if (member == null)
				continue;
			BPPlayer bpMember = BPPlayer.get(member);
			bpMember.setClan(null);
			member.sendMessage(MessageType.CLAN_OTHERBREAKUP.getTranslation().getValue(new Object[0]));
		}
		Player leader = Bukkit.getPlayerExact((String) this.getLeaderName());
		if (leader != null) {
			BPPlayer bpLeader = BPPlayer.get(leader);
			bpLeader.setClan(null);
			leader.sendMessage(MessageType.CLAN_OTHERBREAKUP.getTranslation().getValue(new Object[0]));
		}
		byWho.sendMessage(MessageType.CLAN_BREAKUP.getTranslation().getValue(new Object[0]));
	}

	public int getPoints() {
		int points = 0;
		for (CWMatchResult result : this.matchResults) {
			if (result.hasWon()) {
				++points;
				continue;
			}
			if (!result.hasLost() || --points >= 0)
				continue;
			points = 0;
		}
		return points;
	}

	public int getWins() {
		int wins = 0;
		for (CWMatchResult result : this.matchResults) {
			if (!result.hasWon())
				continue;
			++wins;
		}
		return wins;
	}

	public int getDraws() {
		int draws = 0;
		for (CWMatchResult result : this.matchResults) {
			if (!result.wasDraw())
				continue;
			++draws;
		}
		return draws;
	}

	public int getLoses() {
		int loses = 0;
		for (CWMatchResult result : this.matchResults) {
			if (!result.hasLost())
				continue;
			++loses;
		}
		return loses;
	}

	public void informLeader(String info) {
		String leaderName = this.getLeaderName();
		Player leader = Bukkit.getPlayerExact((String) leaderName);
		if (leader != null) {
			leader.sendMessage(info);
		}
	}

	public void informMembers(String info) {
		for (String memberName : this.getMemberNames()) {
			Player member = Bukkit.getPlayerExact((String) memberName);
			if (member == null)
				continue;
			member.sendMessage(info);
		}
	}

	public void informPlayers(String info) {
		this.informLeader(info);
		this.informMembers(info);
	}

	public void addResult(CWMatchResult result) {
		this.matchResults.add(0, result);
	}

	public File getFile() {
		return new File("plugins/Breakpoint/clans/" + this.name + ".yml");
	}

	public ClanChallenge getPendingChallengeFrom(Clan clan) {
		for (ClanChallenge challenge : this.pendingChallenges) {
			if (!challenge.getChallengingClan().equals(clan))
				continue;
			return challenge;
		}
		return null;
	}

	public Player getLeader() {
		return Bukkit.getPlayerExact((String) this.leader);
	}

	public BPPlayer getBPLeader() {
		Player leader = this.getLeader();
		return leader == null ? null : BPPlayer.get(leader);
	}

	public List<Player> getOnlinePlayers() {
		ArrayList<Player> online = new ArrayList<Player>();
		List<String> names = this.getPlayerNames();
		for (String name : names) {
			Player player = Bukkit.getPlayerExact((String) name);
			if (player == null)
				continue;
			online.add(player);
		}
		return online;
	}

	public List<String> getPlayerNames() {
		ArrayList<String> players = new ArrayList<String>();
		players.add(this.leader);
		players.addAll(this.getModeratorNames());
		players.addAll(this.getMemberNames());
		return players;
	}

	public String getLeaderName() {
		return this.leader;
	}

	public List<String> getModeratorNames() {
		return this.moderators;
	}

	public List<String> getMemberNames() {
		return this.members;
	}

	public List<String> getInvitedNames() {
		return this.invited;
	}

	public void addModerator(String playerName) {
		this.moderators.add(playerName);
	}

	public boolean removeModerator(String playerName) {
		return this.moderators.remove(playerName);
	}

	public void addMember(String playerName) {
		this.members.add(playerName);
	}

	public boolean removeMember(String playerName) {
		return this.members.remove(playerName);
	}

	public void addInvited(String playerName) {
		this.invited.add(playerName);
	}

	public boolean removeInvited(String playerName) {
		return this.invited.remove(playerName);
	}

	public final void setColoredNameRaw(String coloredNameRaw) {
		String coloredName = Clan.getColored(coloredNameRaw);
		String name = ChatColor.stripColor((String) coloredName);
		if (!Clan.isCorrectLength(name)) {
			throw new IllegalArgumentException(
					MessageType.COMMAND_CLAN_RENAME_EXE_INCORRECTLENGTH.getTranslation().getValue(3, 10));
		}
		if (!Clan.hasCorrectCharacters(name)) {
			throw new IllegalArgumentException(
					MessageType.COMMAND_CLAN_RENAME_EXE_BANNEDCHARACTERS.getTranslation().getValue(new Object[0]));
		}
		if (Clan.exists(name)) {
			throw new IllegalArgumentException(
					MessageType.COMMAND_CLAN_RENAME_EXE_ALREADYEXISTS.getTranslation().getValue(name));
		}
		this.coloredNameRaw = coloredNameRaw;
		this.coloredName = coloredName;
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public String getColoredName() {
		return this.coloredName;
	}

	public String getColoredNameRaw() {
		return this.coloredNameRaw;
	}

	public boolean isAtLeastModerator(String playerName) {
		return this.isLeader(playerName) || this.isModerator(playerName);
	}

	public boolean isAtLeastMember(String playerName) {
		return this.isAtLeastModerator(playerName) || this.isMember(playerName);
	}

	public boolean isAtLeastInvited(String playerName) {
		return this.isAtLeastMember(playerName) || this.isInvited(playerName);
	}

	public boolean isLeader(String playerName) {
		return this.leader.equals(playerName);
	}

	public boolean isModerator(String playerName) {
		return this.moderators.contains(playerName);
	}

	public boolean isMember(String playerName) {
		return this.members.contains(playerName);
	}

	public boolean isInvited(String playerName) {
		return this.invited.contains(playerName);
	}

	public LinkedList<ClanChallenge> getPendingChallenges() {
		return this.pendingChallenges;
	}

	public LinkedList<CWMatchResult> getMatchResults() {
		return this.matchResults;
	}
}
