/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.nametagedit.plugin.NametagEdit
 *  com.nametagedit.plugin.api.INametagApi
 *  org.bukkit.Bukkit
 *  org.bukkit.entity.Player
 *  org.bukkit.plugin.Plugin
 */
package cz.GravelCZLP.Breakpoint.hooks;

import com.nametagedit.plugin.NametagEdit;
import com.nametagedit.plugin.api.INametagApi;
import cz.GravelCZLP.Breakpoint.Breakpoint;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class NametagAPIHooks {
	private boolean isHooked = false;
	private INametagApi nametagAPI;

	public static NametagAPIHooks hook() {
		Plugin nametagEdit = Bukkit.getPluginManager().getPlugin("NametagEdit");
		if (nametagEdit == null) {
			Breakpoint.getInstance().getLogger().log(Level.SEVERE,
					"NametagEdit was not found, there will not be any colored names in Games, this can confuse Players.");
			return new NametagAPIHooks();
		}
		NametagAPIHooks hook = new NametagAPIHooks();
		hook.tryHook();
		return hook;
	}

	public void tryHook() {
		this.nametagAPI = NametagEdit.getApi();
		this.isHooked = true;
	}

	public boolean isHooked() {
		return this.isHooked;
	}

	public INametagApi getAPI() {
		return this.nametagAPI;
	}

	public void setPrefix(Player p, String prefix) {
		if (!this.isHooked()) {
			return;
		}
		this.getAPI().setPrefix(p, prefix);
	}

	public void setSuffix(Player p, String suffix) {
		if (!this.isHooked()) {
			return;
		}
		this.getAPI().setSuffix(p, suffix);
	}

	public void updateNametag(BPPlayer bpPlayer) {
		if (!this.isHooked()) {
			return;
		}
		String prefix = bpPlayer.getTagPrefix(true);
		this.setPrefix(bpPlayer.getPlayer(), prefix);
	}
}
