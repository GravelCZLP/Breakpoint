/*
 * Decompiled with CFR 0_132.
 */
package cz.GravelCZLP.Breakpoint.language;

import cz.GravelCZLP.Breakpoint.language.Translateable;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Translation {
	private final Translateable key;
	private final String message;

	protected Translation(Translateable key) {
		this.key = key;
		this.message = null;
	}

	public Translation(Translateable key, String message) {
		this.key = key;
		this.message = message.replace("\\n", "\n");
	}

	public Translateable getKey() {
		return this.key;
	}

	public /* varargs */ String getValue(Object... values) {
		String filled = this.getMessage();
		for (int i = 0; i < values.length; ++i) {
			filled = filled.replace("{" + (i + 1) + "}", values[i].toString());
		}
		return filled;
	}

	public /* varargs */ List<String> getValues(Object... values) {
		String filled = this.getValue(values);
		return Arrays.asList(filled.split("\n"));
	}

	public /* varargs */ void addValuesToList(List<String> pre, Object... values) {
		pre.addAll(this.getValues(values));
	}

	private String getMessage() {
		return this.message;
	}
}
