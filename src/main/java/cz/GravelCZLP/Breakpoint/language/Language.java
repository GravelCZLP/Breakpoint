/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.configuration.file.YamlConfiguration
 */
package cz.GravelCZLP.Breakpoint.language;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

import cz.GravelCZLP.Breakpoint.achievements.AchievementTranslation;
import cz.GravelCZLP.Breakpoint.achievements.AchievementTranslation.TranslationPart;
import cz.GravelCZLP.Breakpoint.achievements.AchievementType;

public class Language {
	public static String languageFileName;

	public static void loadLanguage(String pluginName, String languageFileName) {
		Language.languageFileName = languageFileName;
		File file = new File("plugins/" + pluginName + "/lang/" + languageFileName + ".yml");

		if (file.isDirectory()) {
			file.delete();
		}

		if (!file.exists()) {
			file.getParentFile().mkdirs();

			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		}

		try {
			Language.load(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void load(File file) throws IOException {
		YamlConfiguration yml = YamlConfiguration.loadConfiguration(file);

		addMissingValues(file, yml);

		for (MessageType type : MessageType.values()) {
			String value = yml.getString(type.getYamlPath());
			value = ChatColor.translateAlternateColorCodes('&', value);
			Translation translation = new Translation(type, value);

			type.setTranslation(translation);
		}

		for (AchievementType at : AchievementType.values()) {
			String path = at.getYamlPath();
			String name = yml.getString(path + TranslationPart.NAME.getSuffix());
			String desc = yml.getString(path + TranslationPart.DESCRIPTION.getSuffix());
			name = ChatColor.translateAlternateColorCodes('&', name);
			desc = ChatColor.translateAlternateColorCodes('&', desc);
			AchievementTranslation translation = new AchievementTranslation(at, name, desc);

			at.setTranslation(translation);
		}
	}

	private static void addMissingValues(File file, YamlConfiguration yml) throws IOException {
		List<Translateable> missing = new ArrayList<>();
		List<AchievementType> missingAC = new ArrayList<>();
		boolean save = false;

		for (MessageType type : MessageType.values()) {
			String path = type.getYamlPath();

			if (!yml.contains(path)) {
				if (!save) {
					save = true;
				}

				missing.add(type);
			}
		}

		for (AchievementType ac : AchievementType.values()) {
			String path = ac.getYamlPath();

			if (!yml.contains(path)) {
				if (!save) {
					save = true;
				}

				missingAC.add(ac);
			}
		}

		if (!save) {
			return;
		}

		for (Translateable type : missing) {
			yml.set(type.getYamlPath(), type.getDefaultTranslation());
		}

		for (AchievementType at : missingAC) {
			for (TranslationPart tp : TranslationPart.values()) {
				yml.set(at.getYamlPath() + tp.getSuffix(), at.getDefaultTranslation(tp));
			}
		}

		yml.save(file);
	}

}
