/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.entity.Player
 *  org.bukkit.event.EventHandler
 *  org.bukkit.event.EventPriority
 *  org.bukkit.event.Listener
 *  org.bukkit.event.player.AsyncPlayerChatEvent
 */
package cz.GravelCZLP.Breakpoint.listeners;

import cz.GravelCZLP.Breakpoint.Breakpoint;
import cz.GravelCZLP.Breakpoint.game.Game;
import cz.GravelCZLP.Breakpoint.game.GameListener;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import cz.GravelCZLP.Breakpoint.players.clans.Clan;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener {
	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		Clan clan;
		String playerName;
		Player player = event.getPlayer();
		BPPlayer bpPlayer = BPPlayer.get(player);
		String message = event.getMessage();
		if (message.equals(bpPlayer.getLastMessage())) {
			event.setCancelled(true);
			return;
		}
		bpPlayer.setLastMessage(message);
		Game game = bpPlayer.getGame();
		boolean cont = false;
		if (game != null) {
			cont = game.getListener().onPlayerChat(event, bpPlayer);
		}
		if (cont) {
			return;
		}
		if (message.contains("hacky") || message.contains("hacker")) {
			event.setCancelled(true);
			player.sendMessage("\u00a7cOd toho je /website report :)");
			return;
		}
		if (message.length() >= 1 && message.charAt(0) == '#' && message.charAt(1) == '#') {
			if (bpPlayer.getPlayer().hasPermission("Breakpoint.chat.staffchat")
					|| bpPlayer.getPlayer().hasPermission("Breakpoint.admin")
					|| bpPlayer.getPlayer().hasPermission("Breakpoint.moderator")
					|| bpPlayer.getPlayer().hasPermission("Breakpoint.helper")) {
				message = message.replaceFirst("##", "");
				event.setCancelled(true);
				String playerName2 = player.getName();
				bpPlayer.sendStaffMessage(message);
				Breakpoint.info("Staff chat: " + playerName2 + ": " + message);
				return;
			}
		} else if (message.length() >= 1 && message.charAt(0) == '@' && message.charAt(1) == '@'
				&& (clan = Clan.getByPlayer(playerName = player.getName())) != null) {
			event.setCancelled(true);
			bpPlayer.sendClanMessage(message);
			Breakpoint.info("Clan [" + clan.getName() + "] chat: " + playerName + ": " + message);
			return;
		}
		String chatPrefix = bpPlayer.getChatPrefix();
		event.setFormat(chatPrefix + "%1$s" + (Object) ChatColor.GRAY + ": %2$s");
	}
}
