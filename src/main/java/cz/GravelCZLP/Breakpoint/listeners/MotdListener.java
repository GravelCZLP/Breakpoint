/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  cz.GravelCZLP.PingAPI.PingAPI
 *  cz.GravelCZLP.PingAPI.PingEvent
 *  cz.GravelCZLP.PingAPI.PingListener
 *  cz.GravelCZLP.PingAPI.PingReply
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.util.CachedServerIcon
 */
package cz.GravelCZLP.Breakpoint.listeners;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.comphenix.protocol.wrappers.WrappedServerPing;
import com.comphenix.protocol.wrappers.WrappedServerPing.CompressedImage;

import cz.GravelCZLP.Breakpoint.Breakpoint;
import cz.GravelCZLP.Breakpoint.game.Game;
import cz.GravelCZLP.Breakpoint.game.GameType;
import cz.GravelCZLP.Breakpoint.game.ctf.CTFGame;
import cz.GravelCZLP.Breakpoint.game.ctf.Team;
import cz.GravelCZLP.Breakpoint.managers.GameManager;

public class MotdListener {
	
	public static PacketAdapter setupPing() {
		PacketAdapter pa = new PacketAdapter(null, ListenerPriority.HIGHEST, PacketType.Status.Server.SERVER_INFO) {
			
			@Override
			public void onPacketSending(PacketEvent event) {
				WrappedServerPing wsp = new WrappedServerPing();
				
				CTFGame ctfGame = null;
				for (Game game : GameManager.getGames()) {
					if (game.getType() != GameType.CTF)
						continue;
					ctfGame = (CTFGame) game;
				}
				int blueId = Team.getId(Team.BLUE);
				int redId = Team.getId(Team.RED);
				int pointsBlue = ctfGame.getFlagManager().getScore()[blueId];
				int pointsRed = ctfGame.getFlagManager().getScore()[redId];
				
				String motd = Breakpoint.getBreakpointConfig().getMotdMessage();
				motd = motd.replaceAll("!!RED!!", String.valueOf(pointsRed));
				motd = motd.replaceAll("!!BLUE!!", String.valueOf(pointsBlue));
				motd = ChatColor.translateAlternateColorCodes((char) '&', (String) motd);
				
				wsp.setMotD(motd);
				
				wsp.setVersionProtocol(340);
				wsp.setVersionName("\u00a7aNovinky (" + Bukkit.getOnlinePlayers().size() + "/" + Bukkit.getMaxPlayers() + ")");
				
				List<WrappedGameProfile> list = new ArrayList<>();
				for (String s : Breakpoint.getBreakpointConfig().getMotdNews()) {
					s = ChatColor.translateAlternateColorCodes((char) '&', (String) s);
					list.add(new WrappedGameProfile(UUID.randomUUID(), s));
				}
				
				wsp.setPlayers(list);
				try {
					FileInputStream in = new FileInputStream(new File("plugins/Breakpoint/images/logo.png"));
					
					wsp.setFavicon(CompressedImage.fromPng(in));	
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				event.getPacket().getServerPings().write(0, wsp);
			}
			
		};
		return pa;
	}

}
