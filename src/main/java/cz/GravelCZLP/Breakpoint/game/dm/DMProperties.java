/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.Location
 *  org.bukkit.entity.Player
 *  org.bukkit.inventory.PlayerInventory
 */
package cz.GravelCZLP.Breakpoint.game.dm;

import cz.GravelCZLP.Breakpoint.game.CharacterType;
import cz.GravelCZLP.Breakpoint.game.Game;
import cz.GravelCZLP.Breakpoint.game.GameProperties;
import cz.GravelCZLP.Breakpoint.game.MapPoll;
import cz.GravelCZLP.Breakpoint.game.dm.DMGame;
import cz.GravelCZLP.Breakpoint.managers.InventoryMenuManager;
import cz.GravelCZLP.Breakpoint.managers.PlayerManager;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.PlayerInventory;

public class DMProperties extends GameProperties {
	private Location spawnedAt;

	public DMProperties(DMGame game, BPPlayer bpPlayer) {
		super(game, bpPlayer);
	}

	public void chooseCharacter(CharacterType ct, boolean spawnPlayer) {
		this.setCharacterType(ct);
		BPPlayer bpPlayer = this.getPlayer();
		if (spawnPlayer) {
			bpPlayer.setArmorWoreSince();
			bpPlayer.spawn();
		}
	}

	public void equip() {
		BPPlayer bpPlayer = this.getPlayer();
		Player player = bpPlayer.getPlayer();
		if (this.isPlaying()) {
			DMGame game = this.getGame();
			bpPlayer.equipArmor();
			this.getCharacterType().equipPlayer(player);
			this.getCharacterType().applyEffects(player);
			InventoryMenuManager.showIngameMenu(bpPlayer);
			if (game.votingInProgress()) {
				String playerName = player.getName();
				if (game.getMapPoll().hasVoted(playerName)) {
					PlayerManager.clearHotBar(player.getInventory());
				} else {
					game.getMapPoll().showOptions(bpPlayer);
					player.updateInventory();
				}
			}
		} else {
			bpPlayer.clearInventory();
		}
	}

	@Override
	public boolean isPlaying() {
		return this.getCharacterType() != null;
	}

	@Override
	public boolean hasSpawnProtection() {
		Location loc;
		Player player;
		BPPlayer bpPlayer = this.getPlayer();
		long spawnTime = bpPlayer.getSpawnTime();
		if (spawnTime >= System.currentTimeMillis() - 3000L
				&& (loc = (player = bpPlayer.getPlayer()).getLocation()).distance(this.spawnedAt) <= 2.0) {
			return true;
		}
		return false;
	}

	@Override
	public String getChatPrefix() {
		return "" + (Object) ChatColor.WHITE;
	}

	@Override
	public String getTagPrefix() {
		return "";
	}

	public Location getSpawnedAt() {
		return this.spawnedAt;
	}

	public void setSpawnedAt(Location spawnedAt) {
		this.spawnedAt = spawnedAt;
	}

	@Override
	public DMGame getGame() {
		return (DMGame) super.getGame();
	}
}
