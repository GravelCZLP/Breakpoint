/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.Location
 *  org.bukkit.block.Block
 *  org.bukkit.event.entity.EntityDamageEvent
 *  org.bukkit.event.entity.EntityShootBowEvent
 *  org.bukkit.event.entity.PlayerDeathEvent
 *  org.bukkit.event.entity.PotionSplashEvent
 *  org.bukkit.event.player.AsyncPlayerChatEvent
 *  org.bukkit.event.player.PlayerInteractEvent
 *  org.bukkit.event.player.PlayerMoveEvent
 *  org.bukkit.event.player.PlayerRespawnEvent
 *  org.bukkit.event.player.PlayerTeleportEvent
 *  org.bukkit.inventory.ItemStack
 */
package cz.GravelCZLP.Breakpoint.game;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;

import cz.GravelCZLP.Breakpoint.players.BPPlayer;

public abstract class GameListener {
	private final Game game;

	public GameListener(Game game, Class<? extends Game> gameClass) {
		if (!game.getClass().equals(gameClass)) {
			throw new IllegalArgumentException("Given game is not applicable.");
		}
		this.game = game;
	}

	public abstract boolean onPlayerChat(AsyncPlayerChatEvent var1, BPPlayer var2);

	public abstract void onPlayerDeath(PlayerDeathEvent var1, BPPlayer var2);

	public abstract void onPlayerRespawn(PlayerRespawnEvent var1, BPPlayer var2, boolean var3);

	public abstract void onEntityDamage(EntityDamageEvent var1);

	public abstract void onPlayerShootBow(EntityShootBowEvent var1, BPPlayer var2);

	public abstract void onPlayerSplashedByPotion(PotionSplashEvent var1, BPPlayer var2, BPPlayer var3);

	public abstract void onPlayerRightClickBlock(PlayerInteractEvent var1, BPPlayer var2);

	public abstract void onPlayerPhysicallyInteractWithBlock(PlayerInteractEvent var1, BPPlayer var2, Block var3);

	public abstract void onPlayerRightClickItem(PlayerInteractEvent var1, BPPlayer var2, ItemStack var3);

	public abstract void onPlayerLeftClickItem(PlayerInteractEvent var1, BPPlayer var2, ItemStack var3);

	public abstract void onPlayerTeleport(PlayerTeleportEvent var1, BPPlayer var2);

	public Game getGame() {
		return this.game;
	}

	public abstract void onPlayerMove(BPPlayer var1, Location var2, Location var3, PlayerMoveEvent var4);
}
