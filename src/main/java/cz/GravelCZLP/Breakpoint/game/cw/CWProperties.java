/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 */
package cz.GravelCZLP.Breakpoint.game.cw;

import cz.GravelCZLP.Breakpoint.game.CharacterType;
import cz.GravelCZLP.Breakpoint.game.ctf.CTFGame;
import cz.GravelCZLP.Breakpoint.game.ctf.CTFProperties;
import cz.GravelCZLP.Breakpoint.game.ctf.Team;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import org.bukkit.ChatColor;

public class CWProperties extends CTFProperties {
	public CWProperties(CTFGame game, BPPlayer bpPlayer, Team team, CharacterType characterType) {
		super(game, bpPlayer, team, characterType);
	}

	public CWProperties(CTFGame game, BPPlayer bpPlayer) {
		super(game, bpPlayer);
	}

	@Override
	public String getChatPrefix() {
		Team team = this.getTeam();
		String nameColor = Team.getChatColor(team);
		return (Object) ChatColor.WHITE + "\u00bb" + nameColor;
	}
}
