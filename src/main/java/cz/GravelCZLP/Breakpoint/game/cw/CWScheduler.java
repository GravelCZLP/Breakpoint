/*
 * Decompiled with CFR 0_132.
 */
package cz.GravelCZLP.Breakpoint.game.cw;

import cz.GravelCZLP.Breakpoint.game.cw.CWGame;
import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.language.Translation;
import cz.GravelCZLP.Breakpoint.players.clans.ClanChallenge;
import java.util.Calendar;
import java.util.LinkedList;

public class CWScheduler {
	private final CWGame game;
	private final LinkedList<ClanChallenge> days;

	public CWScheduler(CWGame game, LinkedList<ClanChallenge> days) {
		if (game == null) {
			throw new IllegalArgumentException("Game Cannot be Null");
		}
		if (days == null) {
			throw new IllegalArgumentException("Days cannot by Null");
		}
		this.game = game;
		this.days = days;
	}

	public CWScheduler(CWGame game) {
		this(game, new LinkedList<ClanChallenge>());
	}

	public ClanChallenge getCurrentDay() {
		int dayOfYear = Calendar.getInstance().get(6);
		return this.getDay(dayOfYear);
	}

	public ClanChallenge getDay(int dayOfYear) {
		for (ClanChallenge day : this.days) {
			if (day.getDayOfYear() != dayOfYear)
				continue;
			return day;
		}
		return null;
	}

	public void addDay(ClanChallenge day) {
		if (day == null) {
			throw new IllegalArgumentException("Day cannot be null");
		}
		if (this.getDay(day.getDayOfYear()) != null) {
			throw new IllegalArgumentException("getDay(day.getDayOfYear()) != null");
		}
		this.days.add(day);
	}

	public static Integer getDayIndex(String name) {
		if (name.equalsIgnoreCase(MessageType.CALENDAR_DAY_SUNDAY.getTranslation().getValue(new Object[0]))) {
			return 1;
		}
		if (name.equalsIgnoreCase(MessageType.CALENDAR_DAY_MONDAY.getTranslation().getValue(new Object[0]))) {
			return 2;
		}
		if (name.equalsIgnoreCase(MessageType.CALENDAR_DAY_TUESDAY.getTranslation().getValue(new Object[0]))) {
			return 3;
		}
		if (name.equalsIgnoreCase(MessageType.CALENDAR_DAY_WEDNESDAY.getTranslation().getValue(new Object[0]))) {
			return 4;
		}
		if (name.equalsIgnoreCase(MessageType.CALENDAR_DAY_THURSDAY.getTranslation().getValue(new Object[0]))) {
			return 5;
		}
		if (name.equalsIgnoreCase(MessageType.CALENDAR_DAY_FRIDAY.getTranslation().getValue(new Object[0]))) {
			return 6;
		}
		if (name.equalsIgnoreCase(MessageType.CALENDAR_DAY_SATURDAY.getTranslation().getValue(new Object[0]))) {
			return 7;
		}
		return null;
	}

	public static String getDayName(int index) {
		switch (index) {
		case 1: {
			return MessageType.CALENDAR_DAY_SUNDAY.getTranslation().getValue(new Object[0]);
		}
		case 2: {
			return MessageType.CALENDAR_DAY_MONDAY.getTranslation().getValue(new Object[0]);
		}
		case 3: {
			return MessageType.CALENDAR_DAY_TUESDAY.getTranslation().getValue(new Object[0]);
		}
		case 4: {
			return MessageType.CALENDAR_DAY_WEDNESDAY.getTranslation().getValue(new Object[0]);
		}
		case 5: {
			return MessageType.CALENDAR_DAY_THURSDAY.getTranslation().getValue(new Object[0]);
		}
		case 6: {
			return MessageType.CALENDAR_DAY_FRIDAY.getTranslation().getValue(new Object[0]);
		}
		case 7: {
			return MessageType.CALENDAR_DAY_SATURDAY.getTranslation().getValue(new Object[0]);
		}
		}
		return null;
	}

	private static int getNextDayOfWeekIn(int dayOfWeek) {
		int currentDayOfWeek = Calendar.getInstance().get(7);
		while (dayOfWeek <= currentDayOfWeek) {
			dayOfWeek += 7;
		}
		return dayOfWeek - currentDayOfWeek;
	}

	public static Integer getNextDayOfYear(int dayOfWeek) {
		Calendar calendar = Calendar.getInstance();
		int nextDayOfWeekIn = CWScheduler.getNextDayOfWeekIn(dayOfWeek);
		int currentDayOfYear = calendar.get(6);
		return currentDayOfYear + nextDayOfWeekIn;
	}

	public LinkedList<ClanChallenge> getDays() {
		return this.days;
	}

	public CWGame getGame() {
		return this.game;
	}
}
