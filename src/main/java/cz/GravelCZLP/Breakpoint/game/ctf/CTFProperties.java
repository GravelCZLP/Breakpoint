/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.ChatColor
 *  org.bukkit.Color
 *  org.bukkit.GameMode
 *  org.bukkit.Location
 *  org.bukkit.entity.Player
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.inventory.meta.LeatherArmorMeta
 */
package cz.GravelCZLP.Breakpoint.game.ctf;

import cz.GravelCZLP.Breakpoint.game.CharacterType;
import cz.GravelCZLP.Breakpoint.game.Game;
import cz.GravelCZLP.Breakpoint.game.GameProperties;
import cz.GravelCZLP.Breakpoint.game.MapPoll;
import cz.GravelCZLP.Breakpoint.game.ctf.CTFGame;
import cz.GravelCZLP.Breakpoint.game.ctf.Team;
import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.language.Translation;
import cz.GravelCZLP.Breakpoint.managers.InventoryMenuManager;
import cz.GravelCZLP.Breakpoint.managers.PlayerManager;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import java.util.Random;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

public class CTFProperties extends GameProperties {
	private Team team;

	public CTFProperties(CTFGame game, BPPlayer bpPlayer, Team team, CharacterType characterType) {
		super(game, bpPlayer, characterType);
		this.team = team;
	}

	public CTFProperties(CTFGame game, BPPlayer bpPlayer) {
		super(game, bpPlayer);
	}

	@Override
	public boolean isPlaying() {
		return this.team != null && this.hasCharacterType();
	}

	@Override
	public boolean hasSpawnProtection() {
		CTFGame game = this.getGame();
		BPPlayer bpPlayer = this.getPlayer();
		long spawnTime = bpPlayer.getSpawnTime();
		if (spawnTime >= System.currentTimeMillis() - (long) (1000 * CTFGame.spawnProtectionSeconds)
				&& this.team != null
				&& (bpPlayer.getPlayer().getLocation()).distance(game.getSpawnLocation(this.team)) <= 8.0) {
			return true;
		}
		return false;
	}

	@Override
	public String getChatPrefix() {
		Team team = this.getTeam();
		String nameColor = Team.getChatColor(team);
		return nameColor;
	}

	@Override
	public String getTagPrefix() {
		CTFGame game = this.getGame();
		Team team = this.getTeam();
		String nameColor = Team.getChatColor(team);
		return nameColor + (game.getFlagManager().isHoldingFlag(getPlayer()) ? "§l" : "");
	}

	public void equip() {
		BPPlayer bpPlayer = this.getPlayer();
		Player player = bpPlayer.getPlayer();
		if (this.isPlaying()) {
			CTFGame game = this.getGame();
			CharacterType characterType = this.getCharacterType();
			bpPlayer.equipArmor();
			this.colorChestplate();
			characterType.equipPlayer(player);
			characterType.applyEffects(player);
			InventoryMenuManager.showIngameMenu(bpPlayer);
			if (game.votingInProgress()) {
				String playerName = player.getName();
				if (game.getMapPoll().hasVoted(playerName)) {
					PlayerManager.clearHotBar(player.getInventory());
				} else {
					game.getMapPoll().showOptions(bpPlayer);
					player.updateInventory();
				}
			}
		} else {
			bpPlayer.clearInventory();
		}
	}

	public void colorChestplate() {
		Color color;
		if (this.team == Team.RED) {
			color = Color.RED;
		} else if (this.team == Team.BLUE) {
			color = Color.BLUE;
		} else {
			return;
		}
		BPPlayer bpPlayer = this.getPlayer();
		Player player = bpPlayer.getPlayer();
		PlayerInventory inv = player.getInventory();
		ItemStack[] contents = inv.getArmorContents();
		ItemStack chestplate = contents[2];
		ItemMeta im = chestplate.getItemMeta();
		if (!(im instanceof LeatherArmorMeta)) {
			return;
		}
		LeatherArmorMeta lam = (LeatherArmorMeta) im;
		lam.setColor(color);
		chestplate.setItemMeta((ItemMeta) lam);
		inv.setArmorContents(contents);
	}

	public void chooseTeam(Team team) {
		BPPlayer bpPlayer = this.getPlayer();
		Player player = bpPlayer.getPlayer();
		GameMode gm = player.getGameMode();
		CTFGame game = this.getGame();
		if (gm != GameMode.ADVENTURE) {
			player.setGameMode(GameMode.ADVENTURE);
		}
		this.setTeam(team);
		bpPlayer.spawn();
		bpPlayer.setPlayerListName();
		game.updateTeamMapViews();
		player.getInventory().clear();
		player.updateInventory();
		if (team == Team.RED) {
			player.sendMessage(MessageType.OTHER_TEAMJOIN_RED.getTranslation().getValue(new Object[0]));
		} else if (team == Team.BLUE) {
			player.sendMessage(MessageType.OTHER_TEAMJOIN_BLUE.getTranslation().getValue(new Object[0]));
		}
	}

	public void chooseRandomTeam() {
		CTFGame game = this.getGame();
		int[] teamSizes = game.getTeamSizes();
		if (teamSizes[0] == teamSizes[1]) {
			int[] scores = game.getFlagManager().getScore();
			if (scores[0] == scores[1]) {
				this.chooseTeam(Team.getById(new Random().nextInt(2)));
			} else if (scores[0] < scores[1]) {
				this.chooseTeam(Team.RED);
			} else {
				this.chooseTeam(Team.BLUE);
			}
		} else if (teamSizes[0] < teamSizes[1]) {
			this.chooseTeam(Team.RED);
		} else {
			this.chooseTeam(Team.BLUE);
		}
	}

	public void chooseCharacter(CharacterType ct, boolean spawnPlayer) {
		this.setCharacterType(ct);
		BPPlayer bpPlayer = this.getPlayer();
		if (spawnPlayer) {
			bpPlayer.setArmorWoreSince();
			bpPlayer.spawn();
		}
	}

	public boolean isEnemy(BPPlayer bpPlayer) {
		return Team.areEnemies(this.team, ((CTFProperties) bpPlayer.getGameProperties()).getTeam());
	}

	public boolean isAlly(BPPlayer bpPlayer) {
		return Team.areAllies(this.team, ((CTFProperties) bpPlayer.getGameProperties()).getTeam());
	}

	public Team getTeam() {
		return this.team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	@Override
	public CTFGame getGame() {
		return (CTFGame) super.getGame();
	}
}
