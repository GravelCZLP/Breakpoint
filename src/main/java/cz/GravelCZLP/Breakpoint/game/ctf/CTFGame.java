/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.Chunk
 *  org.bukkit.Color
 *  org.bukkit.FireworkEffect
 *  org.bukkit.FireworkEffect$Builder
 *  org.bukkit.FireworkEffect$Type
 *  org.bukkit.Location
 *  org.bukkit.Material
 *  org.bukkit.OfflinePlayer
 *  org.bukkit.Sound
 *  org.bukkit.World
 *  org.bukkit.block.Block
 *  org.bukkit.command.CommandSender
 *  org.bukkit.configuration.file.YamlConfiguration
 *  org.bukkit.entity.EnderCrystal
 *  org.bukkit.entity.Firework
 *  org.bukkit.entity.Player
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.PlayerInventory
 *  org.bukkit.inventory.meta.FireworkMeta
 *  org.bukkit.map.MapView
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.scheduler.BukkitScheduler
 *  org.bukkit.scoreboard.Objective
 *  org.bukkit.scoreboard.Score
 */
package cz.GravelCZLP.Breakpoint.game.ctf;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.map.MapView;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitScheduler;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;

import cz.GravelCZLP.Breakpoint.Breakpoint;
import cz.GravelCZLP.Breakpoint.game.BPMap;
import cz.GravelCZLP.Breakpoint.game.CharacterType;
import cz.GravelCZLP.Breakpoint.game.Game;
import cz.GravelCZLP.Breakpoint.game.GameType;
import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.language.Translation;
import cz.GravelCZLP.Breakpoint.managers.PlayerManager;
import cz.GravelCZLP.Breakpoint.managers.SBManager;
import cz.GravelCZLP.Breakpoint.managers.SoundManager;
import cz.GravelCZLP.Breakpoint.maps.BPMapPalette;
import cz.GravelCZLP.Breakpoint.maps.MapManager;
import cz.GravelCZLP.Breakpoint.maps.SizeRenderer;
import cz.GravelCZLP.Breakpoint.perks.Perk;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import cz.GravelCZLP.Breakpoint.sound.BPSound;

public class CTFGame extends Game {
	private final FlagManager flm = new FlagManager(this);
	private final TeamBalanceManager tbm = new TeamBalanceManager(this);
	public static final int emeraldsForWin = 50;
	public static final int emeraldsForCapture = 10;
	public static int spawnProtectionSeconds = 10;
	public Location teamSelectionLocation;
	public Location characterSelectionLocation;
	public final SizeRenderer[] teamSizeRenderers = new SizeRenderer[2];
	public final short teamSizeRenderersMapId;
	private final Translation scoreHeaderTranslation = MessageType.SCOREBOARD_PROGRESS_CTF_HEADER.getTranslation();
	private final String[] scoreHeaderNames = new String[] {
			MessageType.SCOREBOARD_PROGRESS_CTF_TEAM_RED.getTranslation().getValue(new Object[0]),
			MessageType.SCOREBOARD_PROGRESS_CTF_TEAM_BLUE.getTranslation().getValue(new Object[0]) };

	@SuppressWarnings("deprecation")
	public CTFGame(GameType gt, String name, Location signLoc, Location teamSelectionLocation,
			Location characterSelectionLocation, LinkedList<CTFMap> maps, boolean balanceTeams) {
		super(gt, name, signLoc, maps);
		if (balanceTeams) {
			this.getTeamBalanceManager().startLoop();
		}
		this.teamSelectionLocation = teamSelectionLocation;
		this.characterSelectionLocation = characterSelectionLocation;
		this.teamSizeRenderersMapId = MapManager.getNextFreeId(2);
		this.teamSizeRenderers[0] = new SizeRenderer(BPMapPalette.getColor(4, 2), BPMapPalette.getColor(4, 0),
				this.getPlayersInTeam(Team.RED).size());
		MapView rtsmv = Bukkit.getMap((short) this.teamSizeRenderersMapId);
		this.teamSizeRenderers[0].set(rtsmv);
		this.teamSizeRenderers[1] = new SizeRenderer(BPMapPalette.getColor(12, 2), BPMapPalette.getColor(12, 0),
				this.getPlayersInTeam(Team.BLUE).size());
		MapView btsmv = Bukkit.getMap(((short) (this.teamSizeRenderersMapId + 1)));
		System.out.print(this.teamSizeRenderersMapId);
		System.out.println(this.teamSizeRenderersMapId + 1);
		this.teamSizeRenderers[1].set(btsmv);
		if (this.players.isEmpty() || this.players.size() <= 1) {
			this.flm.removeFlags();
		}
	}

	public CTFGame(String name, Location signLoc, Location teamSelectionLocation, Location characterSelectionLocation,
			LinkedList<CTFMap> maps) {
		this(GameType.CTF, name, signLoc, teamSelectionLocation, characterSelectionLocation, maps, true);
	}

	public CTFGame(String name, Location signLoc) {
		this(name, signLoc, null, null, new LinkedList<CTFMap>());
	}

	@Override
	public void startExtra() {
		for (BPMap map : this.getPlayableMaps()) {
			CTFMap ctfMap;
			if (map.getGameType() != GameType.CTF || !(ctfMap = (CTFMap) map).isPlayable())
				continue;
			for (int i = 0; i < ctfMap.getTeamFlags().length; ++i) {
				Chunk chunk = ctfMap.getTeamFlags()[i].getWorld().getChunkAt(ctfMap.getTeamFlags()[i]);
				if (chunk.isLoaded())
					continue;
				chunk.load();
			}
		}
		this.flm.startLoops();
	}

	@Override
	public void showInGameMenu(BPPlayer bpPlayer) {
		Player player = bpPlayer.getPlayer();
		PlayerInventory pi = player.getInventory();
		FlagManager.giveCompass(player, pi);
	}

	@Override
	public void onPlayerLeaveGame(BPPlayer bpPlayer) {
		if (this.flm.isHoldingFlag(bpPlayer)) {
			this.flm.dropFlag(bpPlayer);
		}
		if (this.players.size() <= 1) {
			this.flm.removeFlags();
			this.flm.removeHolders();
		}
		super.onPlayerLeaveGame(bpPlayer);
		this.updateTeamMapViews();
	}

	@Override
	public void join(BPPlayer bpPlayer) throws Exception {
		bpPlayer.setGameProperties(new CTFProperties(this, bpPlayer));
		super.join(bpPlayer);
		bpPlayer.spawn();
		this.updateTeamSizeRenderers();
		if (this.players.size() >= 2) {
			if (!this.votingInProgress()) {
				if (!this.flm.isFlagBeingHeld(Team.RED)) {
					this.flm.spawnFlagAtDefaultLocation(Team.RED);
				}
				if (!this.flm.isFlagBeingHeld(Team.BLUE)) {
					this.flm.spawnFlagAtDefaultLocation(Team.BLUE);
				}
			}
		} else {
			this.flm.removeFlags();
			this.flm.removeHolders();
		}
	}

	public void superJoin(BPPlayer bpPlayer) throws Exception {
		super.join(bpPlayer);
	}

	@Override
	public void onCommand(CommandSender sender, String[] args) {
		if (args.length <= 0) {
			sender.sendMessage("info, start, map, teamSelLoc, charSelLoc, spawnFlags, removeFlags, giveNeededMaps");
		} else if (args[0].equalsIgnoreCase("info")) {
			sender.sendMessage("Name: " + getName());
			sender.sendMessage("Active: " + isActive());
			sender.sendMessage("Playable: " + isPlayable(true));
			sender.sendMessage("MapSecondsLeft: " + getMapSecondsLeft());
			sender.sendMessage("#Maps: " + getMaps().size());
			sender.sendMessage("Current map: " + getCurrentMap().getName());
			sender.sendMessage("#Players: " + getPlayers().size());
		} else if (args[0].equalsIgnoreCase("start")) {
			if (isPlayable(true)) {
				start();
			} else {
				sender.sendMessage(ChatColor.RED + "The game is not playable!");
			}
		} else if (args[0].equalsIgnoreCase("teamSelLoc")) {
			if (!(sender instanceof Player)) {
				return;
			}
			Player player = (Player) sender;
			Location loc = player.getLocation();
			this.teamSelectionLocation = loc;
			sender.sendMessage(ChatColor.GREEN + "Team selection location successfully set!");
		} else if (args[0].equalsIgnoreCase("charSelLoc")) {
			if (!(sender instanceof Player)) {
				return;
			}
			Player player = (Player) sender;
			Location loc = player.getLocation();
			this.characterSelectionLocation = loc;
			sender.sendMessage(ChatColor.GREEN + "Character selection location successfully set!");
		} else if (args[0].equalsIgnoreCase("spawnFlags")) {
			this.flm.spawnFlags();
			sender.sendMessage("Flags have been spawned.");
		} else if (args[0].equalsIgnoreCase("removeFlags")) {
			this.flm.removeFlags();
			sender.sendMessage("Flags have been removed.");
		} else if (args[0].equalsIgnoreCase("map")) {
			if (args.length <= 1) {
				sender.sendMessage("list");
				sender.sendMessage("add [MinPlayers] [MaxPlayers] [Name]");
				sender.sendMessage("remove [Name]");
				sender.sendMessage("setCapacity [Min] [Max] [Name]");
				sender.sendMessage("teamSpawn [Team] [Name]");
				sender.sendMessage("teamFlag [Team] [Name]");
				sender.sendMessage("addMelounBoost [Name]");
			} else if (args[1].equalsIgnoreCase("list")) {
				List<? extends BPMap> maps = getMaps();
				for (int i = 0; i < maps.size(); i++) {
					BPMap map = maps.get(i);
					String name = map.getName();
					boolean playable = map.isPlayable();
					boolean capPlayable = map.isPlayableWith(this.players.size());
					int min = map.getMinimumPlayers();
					int max = map.getMaximumPlayers();
					ChatColor color = !playable ? ChatColor.RED : !capPlayable ? ChatColor.YELLOW : ChatColor.GREEN;
					sender.sendMessage(color + name + ChatColor.GRAY + " <" + min + "; " + max + ">");
				}
			} else if (args[1].equalsIgnoreCase("add")) {
				if (args.length <= 4) {
					sender.sendMessage("add [MinPlayers] [MaxPlayers] [Name]");
					return;
				}
				try {
					int min = Integer.parseInt(args[2]);
					int max = Integer.parseInt(args[3]);
					String name = args[4];
					for (int i = 5; i < args.length; i++) {
						name += " " + args[i];
					}
					CTFMap map = new CTFMap(name, min, max);
					getMaps().add(map);
					sender.sendMessage(
							ChatColor.GREEN + "Map '" + name + "' <" + min + "; " + max + "> successfully added.");
				} catch (Exception e) {
					e.printStackTrace();
					sender.sendMessage(ChatColor.RED + "An error occured.");
				}
			} else if (args[1].equalsIgnoreCase("remove")) {
				if (args.length <= 2) {
					sender.sendMessage("remove [Name]");
					return;
				}
				String name = args[2];
				for (int i = 3; i < args.length; i++) {
					name += " " + args[i];
				}
				BPMap map = getMapByName(name);
				getMaps().remove(map);
				sender.sendMessage(ChatColor.GREEN + "Map '" + name + "' successfully removed.");
			} else if (args[1].equalsIgnoreCase("setCapacity")) {
				if (args.length <= 4) {
					sender.sendMessage("setCapacity [MinPlayers] [MaxPlayers] [Name]");
					return;
				}
				String name = args[4];
				int minPlayers, maxPlayers;
				for (int i = 5; i < args.length; i++) {
					name += " " + args[i];
				}
				CTFMap map = getMapByName(name);
				if (map == null) {
					sender.sendMessage(ChatColor.RED + "Map '" + name + "' not found.");
					return;
				}
				try {
					minPlayers = Integer.parseInt(args[2]);
					maxPlayers = Integer.parseInt(args[3]);
				} catch (Exception e) {
					sender.sendMessage(ChatColor.RED + "An error occured.");
					return;
				}
				map.setMinimumPlayers(minPlayers);
				map.setMaximumPlayers(maxPlayers);
				sender.sendMessage(ChatColor.GREEN + "Capacity for map '" + name + "' successfully set. <" + minPlayers
						+ "; " + maxPlayers + ">");
			} else if (args[1].equalsIgnoreCase("teamSpawn")) {
				if (!(sender instanceof Player)) {
					sender.sendMessage(ChatColor.RED + "Only for players!");
					return;
				}
				Player player = (Player) sender;
				if (args.length < 4) {
					return;
				}
				String mapName = args[3];
				for (int i = 4; i < args.length; i++) {
					mapName += " " + args[i];
				}
				CTFMap map = getMapByName(mapName);
				if (map == null) {
					sender.sendMessage(ChatColor.RED + "Map not found!");
					return;
				}
				Team team;
				try {
					team = Team.valueOf(args[2].toUpperCase());
				} catch (Exception e) {
					sender.sendMessage(ChatColor.RED + "Incorrect team");
					return;
				}
				int teamId = Team.getId(team);
				Location[] spawnPoints = map.getTeamSpawn();
				spawnPoints[teamId] = player.getLocation();
				sender.sendMessage(ChatColor.GREEN + "Spawn location on map '" + mapName + "' for team " + teamId
						+ " successfully set.");
			} else if (args[1].equalsIgnoreCase("teamFlag")) {
				if (!(sender instanceof Player)) {
					sender.sendMessage(ChatColor.RED + "Only for players!");
					return;
				}

				Player player = (Player) sender;

				if (args.length < 4) {
					return;
				}

				String mapName = args[3];

				for (int i = 4; i < args.length; i++) {
					mapName += " " + args[i];
				}

				CTFMap map = getMapByName(mapName);

				if (map == null) {
					sender.sendMessage(ChatColor.RED + "Map not found!");
					return;
				}

				Team team;

				try {
					team = Team.valueOf(args[2].toUpperCase());
				} catch (Exception e) {
					sender.sendMessage(ChatColor.RED + "Incorrect team");
					return;
				}

				int teamId = Team.getId(team);
				Location[] spawnFlags = map.getTeamFlags();
				spawnFlags[teamId] = player.getLocation().getBlock().getLocation();
				sender.sendMessage(ChatColor.GREEN + "Flag location on map '" + mapName + "' for team " + teamId
						+ " successfully set.");
			} else if (args[1].equalsIgnoreCase("addMelounBoost")) {
				if (!(sender instanceof Player)) {
					sender.sendMessage(ChatColor.RED + "Only for players!");
					return;
				}
				CTFMap map = getMapByName(args[2]);
				if (map == null) {
					sender.sendMessage(ChatColor.RED + "Map not found!");
					return;
				}
				CTFMap ctfMap;
				if (!(map instanceof CTFMap)) {
					return;
				}
				ctfMap = map;
				Player p = (Player) sender;
				ctfMap.addMelounBoostLocation(p.getLocation());
				sender.sendMessage(ChatColor.GREEN + "Location succesfully added !");
			} else if (args[1].equalsIgnoreCase("giveNeededMaps")) {
				if (!(sender instanceof Player)) {
					sender.sendMessage("Only for Players");
					return;
				}
				List<Short> maps = new ArrayList<Short>();
				for (int i = 0; i < 1; i++) {
					maps.add((short) (teamSizeRenderersMapId + i));
				}
				maps.add(playerAmountRendererMapId);
				maps.add(currentMapMapId);
				for (int i = 0; i < maps.size(); i++) {
					ItemStack map = new ItemStack(Material.MAP, 1, maps.get(i));
					((Player) sender).getInventory().addItem(map);
				}
			}
		}
	}

	@Override
	public void updateLobbyMaps(BPPlayer bpPlayer) {
		super.updateLobbyMaps(bpPlayer);
		Player player = bpPlayer.getPlayer();
		for (int i = 0; i < 1; ++i) {
			@SuppressWarnings("deprecation")
			MapView v = Bukkit.getMap((short) ((this.teamSizeRenderersMapId + i)));
			if (v == null) {
				return;
			}
			player.sendMap(v);
		}
	}

	@Override
	public void updateProgressObjective(BPPlayer bpPlayer) {
		this.updateProgressObjective(bpPlayer.getScoreboardManager().getProgressObj());
	}

	public void updateProgressObjective(Objective progressObj) {
		this.updateProgressObjectiveHeader(progressObj);
		this.updateProgressObjectiveScores(progressObj);
	}

	public void updateProgressObjectiveScores(BPPlayer bpPlayer) {
		this.updateProgressObjectiveScores(bpPlayer.getScoreboardManager().getProgressObj());
	}

	public void updateProgressObjectiveScores(Objective progressObj) {
		for (int i = 0; i < 2; ++i) {
			Score score = progressObj.getScore(this.scoreHeaderNames[i]);
			score.setScore(this.flm.getScore()[i]);
		}
	}

	@Override
	public void updateProgressObjectiveHeader(BPPlayer bpPlayer) {
		this.updateProgressObjectiveHeader(bpPlayer.getScoreboardManager().getProgressObj());
	}

	public void updateProgressObjectiveHeader(Objective progressObj) {
		progressObj
				.setDisplayName(this.scoreHeaderTranslation.getValue(SBManager.formatTime(this.getMapSecondsLeft())));
	}

	public void updateTeamSizeRenderers() {
		for (int i = 0; i < 2; ++i) {
			Team team = Team.getById(i);
			this.teamSizeRenderers[i].setSize(this.getPlayersInTeam(team).size());
			MapManager.updateMapForNotPlayingPlayers((short) (this.teamSizeRenderersMapId + i));
		}
	}

	public void updateProgressObjectiveScore() {
		int[] flagScores = this.getFlagManager().getScore();
		String[] fakePlayers = new String[] {
				MessageType.SCOREBOARD_PROGRESS_CTF_TEAM_RED.getTranslation().getValue(),
				MessageType.SCOREBOARD_PROGRESS_CTF_TEAM_BLUE.getTranslation().getValue() };
		
		for (BPPlayer bpPlayer : this.players) {
			SBManager sbm = bpPlayer.getScoreboardManager();
			Objective progressObj = sbm.getProgressObj();
			for (int i = 0; i < fakePlayers.length; ++i) {
				Score score = progressObj.getScore(fakePlayers[i]);
				score.setScore(flagScores[i]);
			}
		}
	}

	@Override
	public boolean isPlayable() {
		if (!super.isPlayable()) {
			return false;
		}
		return this.flm != null && this.teamSelectionLocation != null && this.characterSelectionLocation != null;
	}

	protected boolean isSuperPlayable() {
		return super.isPlayable();
	}

	@Override
	protected void saveExtra(YamlConfiguration yml) {
		yml.set(this.getName() + ".teamSelLoc",
				(Object) (this.teamSelectionLocation.getWorld().getName() + "," + this.teamSelectionLocation.getX()
						+ "," + this.teamSelectionLocation.getY() + "," + this.teamSelectionLocation.getZ() + ","
						+ this.teamSelectionLocation.getYaw() + "," + this.teamSelectionLocation.getPitch()));
		yml.set(this.getName() + ".charSelLoc",
				(Object) (this.characterSelectionLocation.getWorld().getName() + ","
						+ this.characterSelectionLocation.getX() + "," + this.characterSelectionLocation.getY() + ","
						+ this.characterSelectionLocation.getZ() + "," + this.characterSelectionLocation.getYaw() + ","
						+ this.characterSelectionLocation.getPitch()));
	}

	@Override
	protected void endRoundExtra() {
		this.flm.removeFlags();
		this.flm.removeHolders();
		int[] score = this.flm.getScore();
		this.broadcast((Object) ChatColor.GRAY + "---------------------------------");
		if (score[0] == score[1]) {
			this.broadcast(MessageType.RESULT_CTF_DRAW.getTranslation().getValue(new Object[0]));
			SoundManager.playTeamSound(this, Sound.ENTITY_ARROW_HIT, 16.0f, 0.5f, Team.RED);
			SoundManager.playTeamSound(this, Sound.ENTITY_ARROW_HIT, 16.0f, 0.5f, Team.BLUE);
		} else if (score[0] > score[1]) {
			this.broadcast(MessageType.RESULT_CTF_WIN_RED.getTranslation().getValue(new Object[0]));
			SoundManager.playTeamSound(this, Sound.ENTITY_ENDERDRAGON_DEATH, 16.0f, 0.5f, Team.BLUE);
			SoundManager.playTeamSound(this, Sound.ENTITY_PLAYER_LEVELUP, 16.0f, 4.0f, Team.RED);
			this.awardPlayersInTeam(Team.RED, 50);
			this.spawnFireworks(Team.RED);
			if (score[1] <= 0) {
				SoundManager.playTeamSound(BPSound.FLAWLESS_VICTORY, this, Team.RED);
				SoundManager.playTeamSound(BPSound.HUMILIATING_DEFEAT, this, Team.BLUE);
			}
		} else if (score[1] > score[0]) {
			this.broadcast(MessageType.RESULT_CTF_WIN_BLUE.getTranslation().getValue(new Object[0]));
			SoundManager.playTeamSound(this, Sound.ENTITY_ENDERDRAGON_DEATH, 16.0f, 0.5f, Team.RED);
			SoundManager.playTeamSound(this, Sound.ENTITY_PLAYER_LEVELUP, 16.0f, 4.0f, Team.BLUE);
			this.awardPlayersInTeam(Team.BLUE, 50);
			this.spawnFireworks(Team.BLUE);
			if (score[0] <= 0) {
				SoundManager.playTeamSound(BPSound.FLAWLESS_VICTORY, this, Team.BLUE);
				SoundManager.playTeamSound(BPSound.HUMILIATING_DEFEAT, this, Team.RED);
			}
		}
		this.broadcast((Object) ChatColor.GRAY + "---------------------------------");
		for (BPPlayer bpPlayer : this.players) {
			bpPlayer.getScoreboardManager().updateSidebarObjective();
		}
	}

	public void spawnFireworks(final Team team) {
		BukkitScheduler bc = Bukkit.getScheduler();
		long delay = 10L;
		int amount = (int) (20L / delay * 30L);
		for (int i = 0; i < amount; ++i) {
			bc.scheduleSyncDelayedTask((Plugin) Breakpoint.getInstance(), new Runnable() {

				@Override
				public void run() {
					Firework fw = PlayerManager.spawnFirework(CTFGame.this.getSpawnLocation(team));
					FireworkMeta fm = fw.getFireworkMeta();
					Random rnd = new Random();
					Color[] colors = new Color[3];
					fm.setPower(1 + rnd.nextInt(3));
					if (team == Team.RED) {
						colors[0] = Color.RED;
						colors[1] = Color.ORANGE;
						colors[2] = Color.YELLOW;
					} else if (team == Team.BLUE) {
						colors[0] = Color.BLUE;
						colors[1] = Color.AQUA;
						colors[2] = Color.NAVY;
					}
					for (int i = 0; i < 1 + rnd.nextInt(5); ++i) {
						int j;
						FireworkEffect.Builder fe = FireworkEffect.builder();
						fe = fe.flicker(rnd.nextBoolean());
						fe = fe.trail(rnd.nextBoolean());
						fe = fe.with(FireworkEffect.Type.values()[rnd.nextInt(5)]);
						ArrayList<Color> color = new ArrayList<Color>();
						ArrayList<Color> fade = new ArrayList<Color>();
						for (j = 0; j < 1 + rnd.nextInt(3); ++j) {
							color.add(colors[rnd.nextInt(3)]);
						}
						for (j = 0; j < 1 + rnd.nextInt(3); ++j) {
							fade.add(colors[rnd.nextInt(3)]);
						}
						fe = fe.withColor(color);
						fe = fe.withFade(fade);
						fm.addEffect(fe.build());
					}
					fw.setFireworkMeta(fm);
				}
			}, delay * (long) i);
		}
	}

	@Override
	public CTFMap getCurrentMap() {
		return (CTFMap) super.getCurrentMap();
	}

	public void awardPlayersInTeam(Team team, int emeralds) {
		for (BPPlayer bpWinner : this.getPlayersInTeam(team)) {
			bpWinner.addMoney(emeralds, true, true);
		}
	}

	public List<BPPlayer> getPlayersInTeam(Team team) {
		ArrayList<BPPlayer> list = new ArrayList<BPPlayer>();
		for (BPPlayer bpPlayer : this.getPlayers()) {
			CTFProperties props = (CTFProperties) bpPlayer.getGameProperties();
			Team curTeam = props.getTeam();
			if (curTeam != team)
				continue;
			list.add(bpPlayer);
		}
		return list;
	}

	@SuppressWarnings("deprecation")
	public void updateTeamMapViews() {
		int[] teamSizes = this.getTeamSizes();
		for (int i = 0; i < 2; ++i) {
			this.teamSizeRenderers[i].setSize(teamSizes[i]);
			MapView map = Bukkit.getMap((short) (this.teamSizeRenderersMapId + i));
			for (BPPlayer bpPlayer : BPPlayer.onlinePlayers) {
				Player player = bpPlayer.getPlayer();
				if (!bpPlayer.isInLobby() || map == null)
					continue;
				player.sendMap(map);
			}
		}
	}

	public Location getSpawnLocation(Team team) {
		int i = this.getActiveMapId();
		if (i == -1) {
			i = 0;
		}
		return this.getMaps().get(i).getTeamSpawn()[Team.getId(team)];
	}

	public int[] getTeamSizes() {
		int[] players = new int[] { 0, 0 };
		for (BPPlayer bpPlayer : this.getPlayers()) {
			Team team = ((CTFProperties) bpPlayer.getGameProperties()).getTeam();
			if (team == null)
				continue;
			int teamId = Team.getId(team);
			int[] arrn = players;
			int n = teamId;
			arrn[n] = arrn[n] + 1;
		}
		return players;
	}

	@Override
	public void spawn(BPPlayer bpPlayer) {
		Player player = bpPlayer.getPlayer();
		if (player.isDead()) {
			return;
		}
		CTFProperties props = (CTFProperties) bpPlayer.getGameProperties();
		Team team = props.getTeam();
		bpPlayer.setSpawnTime(System.currentTimeMillis());
		bpPlayer.purify();
		props.equip();
		Perk.onSpawn(bpPlayer);
		if (team == null) {
			bpPlayer.teleport(this.teamSelectionLocation);
			return;
		}
		CharacterType ct = props.getCharacterType();
		if (ct == null) {
			bpPlayer.teleport(this.characterSelectionLocation);
			return;
		}
		Location spawnLoc = this.getSpawnLocation(team);
		bpPlayer.teleport(spawnLoc);
		bpPlayer.getScoreboardManager().updateSidebarObjective();
		this.updateProgressObjective(bpPlayer);
	}

	@Override
	public void reset(BPPlayer bpPlayer) {
		if (this.flm.isHoldingFlag(bpPlayer)) {
			this.flm.dropFlag(bpPlayer);
		}
	}

	@Override
	public void broadcastDeathMessage(String victim) {
		if (this.votingInProgress()) {
			return;
		}
		for (Player player : Bukkit.getOnlinePlayers()) {
			BPPlayer bpPlayer = BPPlayer.get(player);
			if (!bpPlayer.isInGame() || !bpPlayer.getSettings().hasDeathMessages())
				continue;
			player.sendMessage(MessageType.PVP_KILLINFO_DIED.getTranslation().getValue(victim));
		}
	}

	public void sendTeamMessage(Player player, String message) {
		BPPlayer bpPlayer = BPPlayer.get(player);
		CTFProperties props = (CTFProperties) bpPlayer.getGameProperties();
		String playerName = player.getName();
		Team team = props.getTeam();
		if (this.isQuickChat(message)) {
			switch (message.charAt(3)) {
			case '1': {
				message = MessageType.QUICKCHAT_1.getTranslation().getValue(new Object[0]);
				break;
			}
			case '2': {
				message = MessageType.QUICKCHAT_2.getTranslation().getValue(new Object[0]);
				break;
			}
			case '3': {
				message = MessageType.QUICKCHAT_3.getTranslation().getValue(new Object[0]);
				break;
			}
			case '4': {
				message = MessageType.QUICKCHAT_4.getTranslation().getValue(new Object[0]);
				break;
			}
			case '5': {
				message = MessageType.QUICKCHAT_5.getTranslation().getValue(new Object[0]);
				break;
			}
			case '6': {
				message = MessageType.QUICKCHAT_6.getTranslation().getValue(new Object[0]);
				break;
			}
			case '7': {
				message = MessageType.QUICKCHAT_7.getTranslation().getValue(new Object[0]);
				break;
			}
			case '8': {
				message = MessageType.QUICKCHAT_8.getTranslation().getValue(new Object[0]);
				break;
			}
			}
		}
		this.sendTeamMessage(playerName, message, team);
	}

	public boolean isQuickChat(String message) {
		if (message.charAt(0) == '&' && message.charAt(1) == '&' && this.isNumber(message.charAt(3))
				&& message.length() == 2) {
			return true;
		}
		return false;
	}

	public boolean isNumber(char c) {
		if (c == '1' || c == '2' || c == '3' || c == '4' || c == '5' || c == '6' || c == '7' || c == '8' || c == '9') {
			return true;
		}
		return false;
	}

	public void sendTeamMessage(String playerName, String message, Team team) {
		for (BPPlayer bpTarget : this.getPlayers()) {
			Player target = bpTarget.getPlayer();
			CTFProperties props = (CTFProperties) bpTarget.getGameProperties();
			Team targetTeam = props.getTeam();
			message = message.replaceFirst("##", "");
			if (targetTeam != team)
				continue;
			target.sendMessage((Object) ChatColor.GREEN + "" + (Object) ChatColor.ITALIC + playerName + ": " + message);
		}
	}

	public boolean canJoinTeam(Team team) {
		int[] players = this.getTeamSizes();
		if (team == Team.RED) {
			int[] arrn = players;
			arrn[0] = arrn[0] + 1;
			return players[0] <= players[1] + 1;
		}
		if (team == Team.BLUE) {
			int[] arrn = players;
			arrn[1] = arrn[1] + 1;
			return players[1] <= players[0] + 1;
		}
		return false;
	}

	public FlagManager getFlagManager() {
		return this.flm;
	}

	@Override
	public CTFMap getMapByName(String name) {
		return (CTFMap) super.getMapByName(name);
	}

	@SuppressWarnings("unchecked")
	public LinkedList<CTFMap> getMaps() {
		LinkedList<? extends BPMap> maps = super.getMaps();
		return (LinkedList<CTFMap>) maps;
	}

	@Override
	protected void changeMapExtra() {
		this.flm.reset();
		this.flm.spawnFlags();
	}

	public TeamBalanceManager getTeamBalanceManager() {
		return this.tbm;
	}

}
