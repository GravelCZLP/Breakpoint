/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.Color
 *  org.bukkit.Location
 *  org.bukkit.Material
 *  org.bukkit.Particle
 *  org.bukkit.entity.Player
 *  org.bukkit.material.MaterialData
 */
package cz.GravelCZLP.Breakpoint.game.ctf;

import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.material.MaterialData;

public enum Team {
	RED(ChatColor.RED, Color.RED, (byte) 14), BLUE(ChatColor.BLUE, Color.BLUE, (byte) 11);

	private final ChatColor chatColor;
	private final Color color;
	private final byte woolColor;

	private Team(ChatColor chatColor, Color color, byte woolColor) {
		this.chatColor = chatColor;
		this.color = color;
		this.woolColor = woolColor;
	}

	public static boolean areEnemies(Team team1, Team team2) {
		if (team1 == null || team2 == null) {
			return false;
		}
		return team1 != team2;
	}

	public static boolean areAllies(Team team1, Team team2) {
		if (team1 == null || team2 == null) {
			return false;
		}
		return team1 == team2;
	}

	public static int getId(Team team) {
		if (team == null) {
			return -1;
		}
		return team.ordinal();
	}

	public static Team getById(int id) {
		Team[] values = Team.values();
		if (id >= 0 && id < values.length) {
			return values[id];
		}
		return null;
	}

	public static Team getOpposite(Team team) {
		if (team == null) {
			return null;
		}
		switch (team) {
		case RED: {
			return BLUE;
		}
		case BLUE: {
			return RED;
		}
		}
		return null;
	}

	private ChatColor getRawChatColor() {
		return this.chatColor;
	}

	public static String getChatColor(Team team) {
		if (team == null) {
			return (Object) ChatColor.WHITE + "";
		}
		return (Object) team.getRawChatColor() + "";
	}

	public Color getColor() {
		return this.color;
	}

	public byte getWoolColor() {
		return this.woolColor;
	}

	@SuppressWarnings("deprecation")
	public void displayDeathEffect(Location loc) {
		Particle par = Particle.BLOCK_CRACK;
		for (Player p : Bukkit.getOnlinePlayers()) {
			BPPlayer bpPlayer = BPPlayer.get(p);
			if (!bpPlayer.isInGame())
				continue;
			p.spawnParticle(par, loc, 10, (Object) new MaterialData(Material.GLASS, this.getWoolColor()));
		}
	}

}
