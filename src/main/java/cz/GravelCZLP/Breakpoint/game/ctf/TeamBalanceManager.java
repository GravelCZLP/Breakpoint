/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.entity.Player
 *  org.bukkit.plugin.Plugin
 */
package cz.GravelCZLP.Breakpoint.game.ctf;

import cz.GravelCZLP.Breakpoint.Breakpoint;
import cz.GravelCZLP.Breakpoint.game.GameProperties;
import cz.GravelCZLP.Breakpoint.game.ctf.CTFGame;
import cz.GravelCZLP.Breakpoint.game.ctf.CTFProperties;
import cz.GravelCZLP.Breakpoint.game.ctf.Team;
import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.language.Translation;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import java.util.List;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class TeamBalanceManager {
	private final CTFGame game;
	private int loopId;

	public TeamBalanceManager(CTFGame game) {
		this.game = game;
	}

	public void checkTeams() {
		Random random = new Random();
		List<BPPlayer> red = this.game.getPlayersInTeam(Team.RED);
		List<BPPlayer> blue = this.game.getPlayersInTeam(Team.BLUE);
		while (red.size() > blue.size() + 1) {
			this.movePlayerToTeam(red.get(random.nextInt(red.size())), Team.BLUE);
			red = this.game.getPlayersInTeam(Team.RED);
			blue = this.game.getPlayersInTeam(Team.BLUE);
		}
		while (blue.size() > red.size() + 1) {
			this.movePlayerToTeam(blue.get(random.nextInt(blue.size())), Team.RED);
			red = this.game.getPlayersInTeam(Team.RED);
			blue = this.game.getPlayersInTeam(Team.BLUE);
		}
	}

	public void movePlayerToTeam(BPPlayer bpPlayer, Team team) {
		Player player = bpPlayer.getPlayer();
		CTFProperties props = (CTFProperties) bpPlayer.getGameProperties();
		FlagManager flm = this.game.getFlagManager();
		props.setTeam(team);
		bpPlayer.spawn();
		if (flm.isHoldingFlag(bpPlayer)) {
			flm.dropFlag(bpPlayer);
		}
		bpPlayer.setPlayerListName();
		player.updateInventory();
		player.sendMessage((Object) ChatColor.DARK_RED + "--- --- --- --- ---");
		if (team == Team.RED) {
			player.sendMessage(MessageType.BALANCE_MOVERED.getTranslation().getValue(new Object[0]));
		} else if (team == Team.BLUE) {
			player.sendMessage(MessageType.BALANCE_MOVEBLUE.getTranslation().getValue(new Object[0]));
		}
		player.sendMessage((Object) ChatColor.DARK_RED + "--- --- --- --- ---");
	}

	public void startLoop() {
		this.loopId = Bukkit.getScheduler().scheduleSyncRepeatingTask((Plugin) Breakpoint.getInstance(),
				new Runnable() {

					@Override
					public void run() {
						TeamBalanceManager.this.checkTeams();
					}
				}, 0L, 600L);
	}

	public CTFGame getGame() {
		return this.game;
	}

	public int getLoopId() {
		return this.loopId;
	}

}
