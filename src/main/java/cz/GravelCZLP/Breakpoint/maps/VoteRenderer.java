/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.entity.Player
 *  org.bukkit.map.MapCanvas
 *  org.bukkit.map.MapFont
 *  org.bukkit.map.MapView
 *  org.bukkit.map.MinecraftFont
 */
package cz.GravelCZLP.Breakpoint.maps;

import cz.GravelCZLP.Breakpoint.game.BPMap;
import cz.GravelCZLP.Breakpoint.maps.BPMapPalette;
import cz.GravelCZLP.Breakpoint.maps.BPMapRenderer;
import java.awt.image.BufferedImage;
import org.bukkit.entity.Player;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapFont;
import org.bukkit.map.MapView;
import org.bukkit.map.MinecraftFont;

public class VoteRenderer extends BPMapRenderer {
	String name;
	byte[][] image;

	public VoteRenderer(BPMap bpMap) {
		this.name = VoteRenderer.removeSpecial(bpMap.getName());
		this.image = VoteRenderer.toBytes(bpMap.getImage());
	}

	public void render(MapView view, MapCanvas canvas, Player player) {
		byte textFrontColor = BPMapPalette.getColor(8, 2);
		byte textBackColor = BPMapPalette.getColor(11, 0);
		int textWidth = 2;
		int textHeight = 2;
		VoteRenderer.drawBytes(canvas, this.image);
		this.drawText(canvas, 3, 3, textWidth, textHeight, (MapFont) MinecraftFont.Font, this.name, textBackColor);
		this.drawText(canvas, 2, 2, textWidth, textHeight, (MapFont) MinecraftFont.Font, this.name, textFrontColor);
	}

	public static String removeSpecial(String string) {
		string = string.replaceAll("\u00e1", "a");
		string = string.replaceAll("\u00ed", "i");
		string = string.replaceAll("\u00e9", "e");
		string = string.replaceAll("\u00fa", "u");
		string = string.replaceAll("\u00f3", "o");
		return string;
	}
}
