/*
 * Decompiled with CFR 0_132.
 */
package cz.GravelCZLP.Breakpoint.maps;

import cz.GravelCZLP.Breakpoint.maps.BPMapRenderer;
import cz.GravelCZLP.Breakpoint.maps.MapManager;
import java.awt.image.BufferedImage;

public class Display {
	private static final int mapSize = 128;
	private final int tileWidth;
	private final int tileHeight;
	private final short topLeftMapId;
	private final byte[][] surface;

	public Display(int tileWidth, int tileHeight) {
		this.tileWidth = tileWidth;
		this.tileHeight = tileHeight;
		this.surface = new byte[128 * tileWidth][128 * tileHeight];
		this.topLeftMapId = MapManager.getNextFreeId(this.getTileAmount());
	}

	public byte[][] getSurface() {
		return this.surface;
	}

	public int getTileAmount() {
		return this.tileWidth * this.tileHeight;
	}

	public byte getColor(int x, int y) {
		return this.surface[x][y];
	}

	public void setColor(int x, int y, byte color) {
		this.surface[x][y] = color;
	}

	public int getTileWidth() {
		return this.tileWidth;
	}

	public int getTileHeight() {
		return this.tileHeight;
	}

	public short getTopLeftMapId() {
		return this.topLeftMapId;
	}

	public static void initializeWithImage(String path) {
		BufferedImage img = BPMapRenderer.getImage(path);
		int surfaceWidth = img.getWidth();
		int surfaceHeight = img.getHeight();
		byte[][] bytes = BPMapRenderer.toBytes(img);
		int tileWidth = surfaceWidth / 128 + (surfaceWidth % 128 > 0 ? 1 : 0);
		int tileHeight = surfaceHeight / 128 + (surfaceHeight % 128 > 0 ? 1 : 0);
		Display display = new Display(tileWidth, tileHeight);
		int startX = (tileWidth * 128 - surfaceWidth) / 2;
		int startY = (tileHeight * 128 - surfaceWidth) / 2;
		for (int x = 0; x < surfaceWidth; ++x) {
			for (int y = 0; y < surfaceHeight; ++y) {
				display.setColor(startX + x, startY + y, bytes[x][y]);
			}
		}
	}
}
