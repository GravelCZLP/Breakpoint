/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.Material
 *  org.bukkit.command.ConsoleCommandSender
 *  org.bukkit.entity.Player
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.map.MapView
 */
package cz.GravelCZLP.Breakpoint.maps;

import cz.GravelCZLP.Breakpoint.game.Game;
import cz.GravelCZLP.Breakpoint.managers.GameManager;
import cz.GravelCZLP.Breakpoint.managers.StatisticsManager;
import cz.GravelCZLP.Breakpoint.maps.BPMapPalette;
import cz.GravelCZLP.Breakpoint.maps.ImageRenderer;
import cz.GravelCZLP.Breakpoint.maps.StatisticRenderer;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import java.util.LinkedList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.map.MapView;

public class MapManager {
	private static short usedIds = 0;
	public static final int playerGraphDelay = 1;
	public static short breakpointMapId;
	public static short vipMapId;
	public static short czechFlagMapId;
	public static short slovakFlagMapId;
	public static short totalPlayersMapId;
	public static short totalKillsMapId;
	public static short totalDeathsMapId;
	public static short totalMoneyMapId;
	public static short totalBoughtMapId;
	public static StatisticRenderer players;
	public static StatisticRenderer kills;
	public static StatisticRenderer deaths;
	public static StatisticRenderer emeralds;
	public static StatisticRenderer boughtitems;

	public static short getNextFreeId(int amount) {
		short id = usedIds;
		usedIds = (short) (usedIds + amount);
		return id;
	}

	public static short getNextFreeId() {
		return MapManager.getNextFreeId(1);
	}

	public void update() {
		players = new StatisticRenderer("Hracu", BPMapPalette.getColor(5, 2)) {

			@Override
			public String getValue() {
				if (StatisticsManager.isUpdating() || !StatisticsManager.hasTotalStats()) {
					return "Nacitam...";
				}
				return Integer.toString(StatisticsManager.getTotalStats().getPlayerAmount());
			}
		};
		players.set(Bukkit.getMap((short) totalPlayersMapId));
		kills = new StatisticRenderer("Zabiti", BPMapPalette.getColor(1, 2)) {

			@Override
			public String getValue() {
				if (StatisticsManager.isUpdating() || !StatisticsManager.hasTotalStats()) {
					return "Nacitam...";
				}
				return Integer.toString(StatisticsManager.getTotalStats().getKills());
			}
		};
		kills.set(Bukkit.getMap((short) totalKillsMapId));
		deaths = new StatisticRenderer("Umrti", BPMapPalette.getColor(4, 2)) {

			@Override
			public String getValue() {
				if (StatisticsManager.isUpdating() || !StatisticsManager.hasTotalStats()) {
					return "Nacitam...";
				}
				return Integer.toString(StatisticsManager.getTotalStats().getDeaths());
			}
		};
		deaths.set(Bukkit.getMap((short) totalDeathsMapId));
		emeralds = new StatisticRenderer("Emeraldu", BPMapPalette.getColor(1, 2)) {

			@Override
			public String getValue() {
				if (StatisticsManager.isUpdating() || !StatisticsManager.hasTotalStats()) {
					return "Nacitam...";
				}
				return Integer.toString(StatisticsManager.getTotalStats().getMoney());
			}
		};
		emeralds.set(Bukkit.getMap((short) totalMoneyMapId));
		boughtitems = new StatisticRenderer("Nakoupeno veci", BPMapPalette.getColor(2, 2)) {

			@Override
			public String getValue() {
				if (StatisticsManager.isUpdating() || !StatisticsManager.hasTotalStats()) {
					return "Nacitam...";
				}
				return Integer.toString(StatisticsManager.getTotalStats().getBought());
			}
		};
		boughtitems.set(Bukkit.getMap((short) totalBoughtMapId));
		for (Player p : Bukkit.getOnlinePlayers()) {
			p.sendMap(Bukkit.getMap((short) totalBoughtMapId));
			p.sendMap(Bukkit.getMap((short) totalDeathsMapId));
			p.sendMap(Bukkit.getMap((short) totalKillsMapId));
			p.sendMap(Bukkit.getMap((short) totalMoneyMapId));
			p.sendMap(Bukkit.getMap((short) totalPlayersMapId));
		}
	}

	public void setup() {
		this.setIds();
		this.setRenderers();
	}

	private void setIds() {
		breakpointMapId = MapManager.getNextFreeId();
		vipMapId = MapManager.getNextFreeId();
		czechFlagMapId = MapManager.getNextFreeId();
		slovakFlagMapId = MapManager.getNextFreeId();
		totalPlayersMapId = MapManager.getNextFreeId();
		totalKillsMapId = MapManager.getNextFreeId();
		totalDeathsMapId = MapManager.getNextFreeId();
		totalMoneyMapId = MapManager.getNextFreeId();
		totalBoughtMapId = MapManager.getNextFreeId();
		ConsoleCommandSender ccs = Bukkit.getConsoleSender();
		ccs.sendMessage("" + breakpointMapId);
		ccs.sendMessage("" + vipMapId);
		ccs.sendMessage("" + czechFlagMapId);
		ccs.sendMessage("" + slovakFlagMapId);
		ccs.sendMessage("" + totalPlayersMapId);
		ccs.sendMessage("" + totalKillsMapId);
		ccs.sendMessage("" + totalDeathsMapId);
		ccs.sendMessage("" + totalMoneyMapId);
		ccs.sendMessage("" + totalBoughtMapId);
	}

	private void setRenderers() {
		new ImageRenderer("plugins/Breakpoint/images/logo.png").set(Bukkit.getMap((short) breakpointMapId));
		new ImageRenderer("plugins/Breakpoint/images/vip.png").set(Bukkit.getMap((short) vipMapId));
		new ImageRenderer("plugins/Breakpoint/images/czech.png").set(Bukkit.getMap((short) czechFlagMapId));
		new ImageRenderer("plugins/Breakpoint/images/slovak.png").set(Bukkit.getMap((short) slovakFlagMapId));
		players = new StatisticRenderer("Hracu", BPMapPalette.getColor(5, 2)) {

			@Override
			public String getValue() {
				if (StatisticsManager.isUpdating() || !StatisticsManager.hasTotalStats()) {
					return "Nacitam...";
				}
				return Integer.toString(StatisticsManager.getTotalStats().getPlayerAmount());
			}
		};
		players.set(Bukkit.getMap((short) totalPlayersMapId));
		kills = new StatisticRenderer("Zabiti", BPMapPalette.getColor(1, 2)) {

			@Override
			public String getValue() {
				if (StatisticsManager.isUpdating() || !StatisticsManager.hasTotalStats()) {
					return "Nacitam...";
				}
				return Integer.toString(StatisticsManager.getTotalStats().getKills());
			}
		};
		kills.set(Bukkit.getMap((short) totalKillsMapId));
		deaths = new StatisticRenderer("Umrti", BPMapPalette.getColor(4, 2)) {

			@Override
			public String getValue() {
				if (StatisticsManager.isUpdating() || !StatisticsManager.hasTotalStats()) {
					return "Nacitam...";
				}
				return Integer.toString(StatisticsManager.getTotalStats().getDeaths());
			}
		};
		deaths.set(Bukkit.getMap((short) totalDeathsMapId));
		emeralds = new StatisticRenderer("Emeraldu", BPMapPalette.getColor(1, 2)) {

			@Override
			public String getValue() {
				if (StatisticsManager.isUpdating() || !StatisticsManager.hasTotalStats()) {
					return "Nacitam...";
				}
				return Integer.toString(StatisticsManager.getTotalStats().getMoney());
			}
		};
		emeralds.set(Bukkit.getMap((short) totalMoneyMapId));
		boughtitems = new StatisticRenderer("Nakoupeno veci", BPMapPalette.getColor(2, 2)) {

			@Override
			public String getValue() {
				if (StatisticsManager.isUpdating() || !StatisticsManager.hasTotalStats()) {
					return "Nacitam...";
				}
				return Integer.toString(StatisticsManager.getTotalStats().getBought());
			}
		};
		boughtitems.set(Bukkit.getMap((short) totalBoughtMapId));
	}

	public static void updateLobbyMapsForPlayer(BPPlayer bpPlayer) {
		for (Game game : GameManager.getGames()) {
			game.updateLobbyMaps(bpPlayer);
		}
	}

	public static void updateLobbyMapsNotPlayingPlayers() {
		for (BPPlayer bpPlayer : BPPlayer.onlinePlayers) {
			if (bpPlayer.isPlaying())
				continue;
			MapManager.updateLobbyMapsForPlayer(bpPlayer);
		}
	}

	public static void updateMapForNotPlayingPlayers(short mapId) {
		for (BPPlayer bpPlayer : BPPlayer.onlinePlayers) {
			if (bpPlayer.isPlaying() || !bpPlayer.isOnline())
				continue;
			MapView v = Bukkit.getMap((short) mapId);
			if (v == null) {
				return;
			}
			bpPlayer.getPlayer().sendMap(v);
		}
	}

	public static ItemStack getMap(Player player, short id) {
		ItemStack is = new ItemStack(Material.MAP, 1, id);
		MapView mw = Bukkit.getMap((short) id);
		player.sendMap(mw);
		return is;
	}

	public static ItemStack getBreakpointMap(Player player) {
		ItemStack is = MapManager.getMap(player, breakpointMapId);
		ItemMeta im = is.getItemMeta();
		im.setDisplayName(
				(Object) ChatColor.DARK_RED + "" + (Object) ChatColor.BOLD + "V\u00edtejte na serveru BREAKPOINT.");
		is.setItemMeta(im);
		return is;
	}

}
