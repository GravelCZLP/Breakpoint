/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.entity.Player
 *  org.bukkit.map.MapCanvas
 *  org.bukkit.map.MapPalette
 *  org.bukkit.map.MapView
 */
package cz.GravelCZLP.Breakpoint.maps;

import cz.GravelCZLP.Breakpoint.maps.BPMapRenderer;
import java.awt.Image;
import java.awt.image.BufferedImage;
import org.bukkit.entity.Player;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapPalette;
import org.bukkit.map.MapView;

public class ImageRenderer extends BPMapRenderer {
	byte[][] image;

	public ImageRenderer(String path) {
		BufferedImage rawImage = ImageRenderer.getImage(path);
		rawImage = MapPalette.resizeImage((Image) rawImage);
		this.image = ImageRenderer.toBytes(rawImage);
	}

	public void render(MapView view, MapCanvas canvas, Player player) {
		ImageRenderer.drawBytes(canvas, this.image);
	}
}
