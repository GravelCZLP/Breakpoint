/*
 * Decompiled with CFR 0_132.
 */
package cz.GravelCZLP.Breakpoint.maps;

import cz.GravelCZLP.Breakpoint.Breakpoint;
import java.awt.Color;

public class BPMapPalette {
	public static final int TRANSPARENT = 0;
	public static final int LIGHT_GREEN = 1;
	public static final int YELLOW = 2;
	public static final int LIGHT_GRAY_1 = 3;
	public static final int RED = 4;
	public static final int LIGHT_BLUE = 5;
	public static final int LIGHT_GRAY_2 = 6;
	public static final int DARK_GREEN = 7;
	public static final int WHITE = 8;
	public static final int LIGHT_GRAY_BLUE = 9;
	public static final int LIGHT_BROWN = 10;
	public static final int DARK_GRAY = 11;
	public static final int DARK_BLUE = 12;
	public static final int DARK_BROWN = 13;
	public static final int baseColorLength = 14;
	public static final Color[] colors = new Color[] { new Color(0, 0, 0, 0), new Color(0, 0, 0, 0),
			new Color(0, 0, 0, 0), new Color(0, 0, 0, 0), BPMapPalette.c(89, 125, 39), BPMapPalette.c(109, 153, 48),
			BPMapPalette.c(27, 178, 56), BPMapPalette.c(109, 153, 48), BPMapPalette.c(174, 164, 115),
			BPMapPalette.c(213, 201, 140), BPMapPalette.c(247, 233, 163), BPMapPalette.c(213, 201, 140),
			BPMapPalette.c(117, 117, 117), BPMapPalette.c(144, 144, 144), BPMapPalette.c(167, 167, 167),
			BPMapPalette.c(144, 144, 144), BPMapPalette.c(180, 0, 0), BPMapPalette.c(220, 0, 0),
			BPMapPalette.c(255, 0, 0), BPMapPalette.c(220, 0, 0), BPMapPalette.c(112, 112, 180),
			BPMapPalette.c(138, 138, 220), BPMapPalette.c(160, 160, 255), BPMapPalette.c(138, 138, 220),
			BPMapPalette.c(117, 117, 117), BPMapPalette.c(144, 144, 144), BPMapPalette.c(167, 167, 167),
			BPMapPalette.c(144, 144, 144), BPMapPalette.c(0, 87, 0), BPMapPalette.c(0, 106, 0),
			BPMapPalette.c(0, 124, 0), BPMapPalette.c(0, 106, 0), BPMapPalette.c(180, 180, 180),
			BPMapPalette.c(220, 220, 220), BPMapPalette.c(255, 255, 255), BPMapPalette.c(220, 220, 220),
			BPMapPalette.c(115, 118, 129), BPMapPalette.c(141, 144, 158), BPMapPalette.c(164, 168, 184),
			BPMapPalette.c(141, 144, 158), BPMapPalette.c(129, 74, 33), BPMapPalette.c(157, 91, 40),
			BPMapPalette.c(183, 106, 47), BPMapPalette.c(157, 91, 40), BPMapPalette.c(79, 79, 79),
			BPMapPalette.c(96, 96, 96), BPMapPalette.c(112, 112, 112), BPMapPalette.c(96, 96, 96),
			BPMapPalette.c(45, 45, 180), BPMapPalette.c(55, 55, 220), BPMapPalette.c(64, 64, 255),
			BPMapPalette.c(55, 55, 220), BPMapPalette.c(73, 58, 35), BPMapPalette.c(89, 71, 43),
			BPMapPalette.c(104, 83, 50), BPMapPalette.c(89, 71, 43) };

	public static byte getColor(int id, int shade) {
		int colorId = id * 4 + shade;
		if (colorId < colors.length) {
			return BPMapPalette.matchColor(colors[id * 4 + shade]);
		}
		Breakpoint.warn("Incorrect color! id: " + id + " shade: " + shade
				+ "; make sure to use BPMapPalette instead of MapPalette!");
		return 0;
	}

	private static Color c(int r, int g, int b) {
		return new Color(r, g, b);
	}

	public static byte matchColor(Color color) {
		if (color.getAlpha() < 128) {
			return 0;
		}
		int index = 0;
		double best = -1.0;
		for (int i = 4; i < colors.length; ++i) {
			double distance = BPMapPalette.getDistance(color, colors[i]);
			if (distance >= best && best != -1.0)
				continue;
			best = distance;
			index = i;
		}
		return (byte) index;
	}

	private static double getDistance(Color c1, Color c2) {
		double rmean = (double) (c1.getRed() + c2.getRed()) / 2.0;
		double r = c1.getRed() - c2.getRed();
		double g = c1.getGreen() - c2.getGreen();
		int b = c1.getBlue() - c2.getBlue();
		double weightR = 2.0 + rmean / 256.0;
		double weightG = 4.0;
		double weightB = 2.0 + (255.0 - rmean) / 256.0;
		return weightR * r * r + weightG * g * g + weightB * (double) b * (double) b;
	}
}
