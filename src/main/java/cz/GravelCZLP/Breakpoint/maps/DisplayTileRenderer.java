/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  org.bukkit.entity.Player
 *  org.bukkit.map.MapCanvas
 *  org.bukkit.map.MapView
 */
package cz.GravelCZLP.Breakpoint.maps;

import cz.GravelCZLP.Breakpoint.maps.BPMapRenderer;
import cz.GravelCZLP.Breakpoint.maps.Display;
import org.bukkit.entity.Player;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapView;

public class DisplayTileRenderer extends BPMapRenderer {
	private final Display display;
	private final int tileX;
	private final int tileY;

	public DisplayTileRenderer(Display display, int tileX, int tileY) {
		this.display = display;
		this.tileX = tileX;
		this.tileY = tileY;
	}

	public void render(MapView view, MapCanvas canvas, Player player) {
		int surfaceX = this.getSurfaceX();
		int surfaceY = this.getSurfaceY();
		for (int x = 0; x < 128; ++x) {
			for (int y = 0; y < 128; ++y) {
				byte color = this.display.getColor(surfaceX + x, surfaceY + y);
				canvas.setPixel(x, y, color);
			}
		}
	}

	public Display getDisplay() {
		return this.display;
	}

	public int getSurfaceX() {
		return this.tileX * 128;
	}

	public int getSurfaceY() {
		return this.tileY * 128;
	}

	public int getTileX() {
		return this.tileX;
	}

	public int getTileY() {
		return this.tileY;
	}
}
