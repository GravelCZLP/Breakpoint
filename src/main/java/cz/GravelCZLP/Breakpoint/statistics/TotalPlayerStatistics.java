/*
 * Decompiled with CFR 0_132.
 */
package cz.GravelCZLP.Breakpoint.statistics;

import cz.GravelCZLP.Breakpoint.game.CharacterType;
import cz.GravelCZLP.Breakpoint.statistics.PlayerStatistics;
import java.util.HashMap;

public class TotalPlayerStatistics extends PlayerStatistics {
	private int playerAmount = 0;

	public TotalPlayerStatistics() {
		super(null, 0, 0, 0, 0, 0, 0, 0, null);
		HashMap<CharacterType, Integer> ctKills = new HashMap<CharacterType, Integer>();
		for (CharacterType ct : CharacterType.values()) {
			ctKills.put(ct, 0);
		}
		this.setCharacterKills(ctKills);
	}

	public void add(PlayerStatistics stat) {
		this.setPlayerAmount(this.getPlayerAmount() + 1);
		this.increaseKills(stat.getKills());
		this.increaseAssists(stat.getAssists());
		this.increaseDeaths(stat.getDeaths());
		this.increaseFlagTakes(stat.getFlagTakes());
		this.increaseFlagCaptures(stat.getFlagCaptures());
		this.increaseBought(stat.getBought());
		this.increaseMoney(stat.getMoney());
		for (CharacterType ct : CharacterType.values()) {
			this.increaseKills(stat.getKills(ct), ct);
		}
	}

	public double getAverageKills() {
		return (double) this.getKills() / (double) this.playerAmount;
	}

	public double getAverageAssists() {
		return (double) this.getAssists() / (double) this.playerAmount;
	}

	public double getAverageDeaths() {
		return (double) this.getDeaths() / (double) this.playerAmount;
	}

	public double getAverageFlagTakes() {
		return (double) this.getFlagTakes() / (double) this.playerAmount;
	}

	public double getAverageFlagCaptures() {
		return (double) this.getFlagCaptures() / (double) this.playerAmount;
	}

	public double getAverageBought() {
		return (double) this.getBought() / (double) this.playerAmount;
	}

	public double getAverageMoney() {
		return (double) this.getMoney() / (double) this.playerAmount;
	}

	public double getAverageKills(CharacterType ct) {
		return (double) this.getKills(ct) / (double) this.playerAmount;
	}

	public int getPlayerAmount() {
		return this.playerAmount;
	}

	public void setPlayerAmount(int playerAmount) {
		this.playerAmount = playerAmount;
	}
}
