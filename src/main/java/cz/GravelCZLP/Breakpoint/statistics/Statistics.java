/*
 * Decompiled with CFR 0_132.
 */
package cz.GravelCZLP.Breakpoint.statistics;

import java.lang.reflect.Field;

public class Statistics {
	private String name;
	private int kills;
	private int assists;
	private int deaths;
	private int money;
	private int bought;
	private int flagTakes;
	private int flagCaptures;

	public Statistics(String name, int kills, int assists, int deaths, int money, int bought, int flagTakes,
			int flagCaptures) {
		this.name = name;
		this.kills = kills;
		this.assists = assists;
		this.deaths = deaths;
		this.money = money;
		this.bought = bought;
		this.flagTakes = flagTakes;
		this.flagCaptures = flagCaptures;
	}

	public void increaseScore(int kills, int deaths) {
		this.increaseKills(kills);
		this.increaseDeaths(deaths);
	}

	public void increaseBought(int by) {
		this.bought += by;
	}

	public void increaseBought() {
		this.increaseBought(1);
	}

	public void increaseAssists(int by) {
		this.assists += by;
	}

	public void increaseAssists() {
		this.increaseAssists(1);
	}

	public void increaseKills(int by) {
		this.kills += by;
	}

	public void increaseKills() {
		this.increaseKills(1);
	}

	public void increaseDeaths(int by) {
		this.deaths += by;
	}

	public void increaseDeaths() {
		this.increaseDeaths(1);
	}

	public void increaseFlagCaptures(int by) {
		this.flagCaptures += by;
	}

	public void increaseFlagCaptures() {
		this.increaseFlagCaptures(1);
	}

	public void increaseFlagTakes(int by) {
		this.flagTakes += by;
	}

	public void increaseFlagTakes() {
		this.increaseFlagTakes(1);
	}

	public void increaseMoney(int by) {
		this.money += by;
	}

	public void increaseMoney() {
		this.increaseMoney(1);
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getKills() {
		return this.kills;
	}

	public void setKills(int kills) {
		this.kills = kills;
	}

	public int getDeaths() {
		return this.deaths;
	}

	public void setDeaths(int deaths) {
		this.deaths = deaths;
	}

	public int getMoney() {
		return this.money;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public int getBought() {
		return this.bought;
	}

	public void setBought(int bought) {
		this.bought = bought;
	}

	public int getFlagTakes() {
		return this.flagTakes;
	}

	public void setFlagTakes(int flagTakes) {
		this.flagTakes = flagTakes;
	}

	public int getFlagCaptures() {
		return this.flagCaptures;
	}

	public void setFlagCaptures(int flagCaptures) {
		this.flagCaptures = flagCaptures;
	}

	public int getAssists() {
		return this.assists;
	}

	public void setAssists(int assists) {
		this.assists = assists;
	}

	public void setDefault() {
		for (Field f : this.getClass().getFields()) {
			if (!f.isAccessible()) {
				f.setAccessible(true);
			}
			if (f.getType() != Integer.TYPE)
				continue;
			try {
				f.setInt(f, 0);
			} catch (IllegalAccessException | IllegalArgumentException e) {
				e.printStackTrace();
			}
		}
	}
}
