/*
 * Decompiled with CFR 0_132.
 */
package cz.GravelCZLP.Breakpoint.statistics;

import com.fijistudios.jordan.FruitSQL;
import cz.GravelCZLP.Breakpoint.Breakpoint;
import cz.GravelCZLP.Breakpoint.Configuration;
import cz.GravelCZLP.Breakpoint.game.CharacterType;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import cz.GravelCZLP.Breakpoint.statistics.Statistics;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import me.limeth.storageAPI.Column;
import me.limeth.storageAPI.ColumnType;
import me.limeth.storageAPI.Storage;
import me.limeth.storageAPI.StorageType;

public class PlayerStatistics extends Statistics {
	private HashMap<CharacterType, Integer> ctKills;

	public PlayerStatistics(String name, int kills, int assists, int deaths, int money, int bought, int flagTakes,
			int flagCaptures, HashMap<CharacterType, Integer> ctKills) {
		super(name, kills, assists, deaths, money, bought, flagTakes, flagCaptures);
		this.ctKills = ctKills;
	}

	public static PlayerStatistics loadPlayerStatistics(String playerName) {
		try {
			Configuration config = Breakpoint.getBreakpointConfig();
			StorageType type = config.getStorageType();
			File dir = BPPlayer.getFolder();
			String table = config.getMySQLTablePlayers();
			FruitSQL mySQL = Breakpoint.getMySQL();
			Storage storage = Storage.load(type, playerName, dir, mySQL, table);
			return PlayerStatistics.loadPlayerStatistics(storage);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static PlayerStatistics loadPlayerStatistics(Storage storage) throws Exception {
		int kills = storage.get(Integer.class, "kills", 0);
		int assists = storage.get(Integer.class, "assists", 0);
		int deaths = storage.get(Integer.class, "deaths", 0);
		int money = storage.get(Integer.class, "money", 0);
		int bought = storage.get(Integer.class, "bought", 0);
		int flagTakes = storage.get(Integer.class, "flagTakes", 0);
		int flagCaptures = storage.get(Integer.class, "flagCaptures", 0);
		HashMap<CharacterType, Integer> ctKills = new HashMap<CharacterType, Integer>();
		for (CharacterType ct : CharacterType.values()) {
			String name = ct.name();
			int curKills = storage.get(Integer.class, "ctKills." + name, 0);
			ctKills.put(ct, curKills);
		}
		return new PlayerStatistics(storage.getName(), kills, assists, deaths, money, bought, flagTakes, flagCaptures,
				ctKills);
	}

	public void savePlayerStatistics(Storage storage) {
		storage.put("kills", (Object) this.getKills());
		storage.put("assists", (Object) this.getAssists());
		storage.put("deaths", (Object) this.getDeaths());
		storage.put("money", (Object) this.getMoney());
		storage.put("bought", (Object) this.getBought());
		storage.put("flagTakes", (Object) this.getFlagTakes());
		storage.put("flagCaptures", (Object) this.getFlagCaptures());
		for (Map.Entry<CharacterType, Integer> entry : this.ctKills.entrySet()) {
			CharacterType ct = entry.getKey();
			int curKills = entry.getValue();
			String name = ct.name();
			storage.put("ctKills." + name, (Object) curKills);
		}
	}

	public static List<Column> getRequiredMySQLColumns() {
		ArrayList<Column> list = new ArrayList<Column>();
		list.add(new Column("kills", ColumnType.INT, new String[0]));
		list.add(new Column("assists", ColumnType.INT, new String[0]));
		list.add(new Column("deaths", ColumnType.INT, new String[0]));
		list.add(new Column("money", ColumnType.INT, new String[0]));
		list.add(new Column("bought", ColumnType.INT, new String[0]));
		list.add(new Column("flagTakes", ColumnType.INT, new String[0]));
		list.add(new Column("flagCaptures", ColumnType.INT, new String[0]));
		for (CharacterType ct : CharacterType.values()) {
			String name = ct.name();
			list.add(new Column("ctKills." + name, ColumnType.INT, new String[0]));
		}
		return list;
	}

	public boolean areDefault() {
		if (this.getKills() != 0 || this.getAssists() != 0 || this.getDeaths() != 0 || this.getMoney() != 0
				|| this.getBought() != 0 || this.getFlagTakes() != 0 || this.getFlagCaptures() != 0) {
			return false;
		}
		for (Integer kills : this.ctKills.values()) {
			if (kills == null || kills == 0)
				continue;
			return false;
		}
		return true;
	}

	public void increaseKills(int by, CharacterType ct) {
		this.ctKills.put(ct, this.ctKills.get((Object) ct) + by);
	}

	public void increaseKills(CharacterType ct) {
		this.increaseKills(1, ct);
	}

	public int getKills(CharacterType ct) {
		return this.ctKills.get((Object) ct);
	}

	public HashMap<CharacterType, Integer> getCharacterKills() {
		return this.ctKills;
	}

	public void setCharacterKills(HashMap<CharacterType, Integer> ctKills) {
		this.ctKills = ctKills;
	}
}
