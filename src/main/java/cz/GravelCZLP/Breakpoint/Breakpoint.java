/*
 * Decompiled with CFR 0_132.
 * 
 * Could not load the following classes:
 *  com.comphenix.protocol.PacketType
 *  com.comphenix.protocol.PacketType$Play
 *  com.comphenix.protocol.PacketType$Play$Server
 *  com.comphenix.protocol.ProtocolLibrary
 *  com.comphenix.protocol.ProtocolManager
 *  com.comphenix.protocol.events.PacketAdapter
 *  com.comphenix.protocol.events.PacketContainer
 *  com.comphenix.protocol.events.PacketEvent
 *  com.comphenix.protocol.events.PacketListener
 *  com.comphenix.protocol.reflect.StructureModifier
 *  com.comphenix.protocol.wrappers.WrappedChatComponent
 *  org.bukkit.Bukkit
 *  org.bukkit.ChatColor
 *  org.bukkit.Location
 *  org.bukkit.Material
 *  org.bukkit.Server
 *  org.bukkit.World
 *  org.bukkit.command.CommandExecutor
 *  org.bukkit.command.ConsoleCommandSender
 *  org.bukkit.command.PluginCommand
 *  org.bukkit.enchantments.Enchantment
 *  org.bukkit.entity.Entity
 *  org.bukkit.entity.Item
 *  org.bukkit.entity.Player
 *  org.bukkit.event.Listener
 *  org.bukkit.inventory.ItemStack
 *  org.bukkit.inventory.meta.ItemMeta
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.plugin.PluginManager
 *  org.bukkit.plugin.java.JavaPlugin
 *  org.bukkit.scheduler.BukkitScheduler
 */
package cz.GravelCZLP.Breakpoint;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.events.PacketListener;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.fijistudios.jordan.FruitSQL;

import cz.GravelCZLP.Breakpoint.hooks.NametagAPIHooks;
import cz.GravelCZLP.Breakpoint.language.Language;
import cz.GravelCZLP.Breakpoint.language.MessageType;
import cz.GravelCZLP.Breakpoint.listeners.BanListener;
import cz.GravelCZLP.Breakpoint.listeners.ChatListener;
import cz.GravelCZLP.Breakpoint.listeners.PVPListener;
import cz.GravelCZLP.Breakpoint.listeners.PlayerConnectionListener;
import cz.GravelCZLP.Breakpoint.listeners.PlayerInteractListener;
import cz.GravelCZLP.Breakpoint.listeners.PlayerInventoryListener;
import cz.GravelCZLP.Breakpoint.managers.AfkManager;
import cz.GravelCZLP.Breakpoint.managers.ChatManager;
import cz.GravelCZLP.Breakpoint.managers.DoubleMoneyManager;
import cz.GravelCZLP.Breakpoint.managers.GameManager;
import cz.GravelCZLP.Breakpoint.managers.InventoryMenuManager;
import cz.GravelCZLP.Breakpoint.managers.Licence;
import cz.GravelCZLP.Breakpoint.managers.LobbyInfoManager;
import cz.GravelCZLP.Breakpoint.managers.StatisticsManager;
import cz.GravelCZLP.Breakpoint.managers.VIPManager;
import cz.GravelCZLP.Breakpoint.managers.commands.AchievementsCommandExecutor;
import cz.GravelCZLP.Breakpoint.managers.commands.BPCommandExecutor;
import cz.GravelCZLP.Breakpoint.managers.commands.BreakpointCommand;
import cz.GravelCZLP.Breakpoint.managers.commands.CWCommandExecutor;
import cz.GravelCZLP.Breakpoint.managers.commands.ClanCommandExecutor;
import cz.GravelCZLP.Breakpoint.managers.commands.FlyCommandExecutor;
import cz.GravelCZLP.Breakpoint.managers.commands.GMCommandExecutor;
import cz.GravelCZLP.Breakpoint.managers.commands.HelpOPCommandExecutor;
import cz.GravelCZLP.Breakpoint.managers.commands.RankCommandExecutor;
import cz.GravelCZLP.Breakpoint.managers.commands.ShopCommand;
import cz.GravelCZLP.Breakpoint.managers.commands.SkullCommandExecutor;
import cz.GravelCZLP.Breakpoint.managers.commands.TopClansCommandExecutor;
import cz.GravelCZLP.Breakpoint.managers.commands.TopCommandExecutor;
import cz.GravelCZLP.Breakpoint.managers.events.EventManager;
import cz.GravelCZLP.Breakpoint.managers.events.advent.AdventManager;
import cz.GravelCZLP.Breakpoint.maps.MapManager;
import cz.GravelCZLP.Breakpoint.players.BPPlayer;
import cz.GravelCZLP.Breakpoint.players.Settings;
import cz.GravelCZLP.Breakpoint.players.clans.Clan;
import me.limeth.storageAPI.StorageType;

public class Breakpoint extends JavaPlugin {
	private static Breakpoint instance;
	private static Configuration config;
	private static FruitSQL mySQL;
	public static final String PLUGIN_NAME = "Breakpoint";
	public AfkManager afkm = new AfkManager(this);
	public MapManager mapm;
	public static BreakpointCommand externalExceturorsHandler;
	public ProtocolManager prm;
	public EventManager evtm;
	public boolean successfullyEnabled;
	public NametagAPIHooks nameTagAPIHook;
	public long timeBoot;
	private boolean canEnable = false;

	public void onEnable() {
		this.canEnable = Licence.isAllowed();
		if (this.canEnable) {
			instance = this;
			this.prm = ProtocolLibrary.getProtocolManager();
			this.mapm = new MapManager();
			this.mapm.setup();
			Clan.loadClans();
			if (Configuration.getFile().exists()) {
				config = Configuration.load();
			} else {
				File f = Configuration.getFile();
				if (f.isDirectory()) {
					f.delete();
				}
				if (!f.exists()) {
					try {
						f.createNewFile();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				config = Configuration.load();
			}
			if (config.getStorageType() == StorageType.MYSQL) {
				mySQL = config.connectToMySQL();
			}
			externalExceturorsHandler = new BreakpointCommand();
			BPPlayer.updateTable(mySQL);
			Language.loadLanguage(PLUGIN_NAME, config.getLanguageFileName());
			config.getRandomShop().build();
			ChatManager.loadStrings();
			InventoryMenuManager.initialize();
			GameManager.loadGames();
			GameManager.startPlayableGames();
			this.redirectCommands();
			StatisticsManager.startLoop();
			this.registerListeners();
			this.afkm.startLoop();
			VIPManager.startLoops();
			LobbyInfoManager.startLoop();
			this.setEventManager();
			DoubleMoneyManager.update();
			DoubleMoneyManager.startBoostLoop();
			StatisticsManager.updateStatistics();
			this.nameTagAPIHook = NametagAPIHooks.hook();
			for (World w : Bukkit.getWorlds()) {
				List<Entity> entites = w.getEntities();
				for (Entity e : entites) {
					if (!(e instanceof Item) || ((Item) e).getItemStack().getType() != Material.SPECKLED_MELON)
						continue;
					e.remove();
				}
			}
			this.getServer().clearRecipes();
			World world = config.getLobbyLocation().getWorld();
			world.setStorm(false);
			world.setThundering(false);
			world.setWeatherDuration(1000000000);
			this.successfullyEnabled = true;
			this.timeBoot = System.currentTimeMillis();
			Bukkit.getScheduler().scheduleSyncRepeatingTask((Plugin) this, (Runnable) new Utils.TPSCounter(), 0L, 1L);
			Bukkit.getScheduler().scheduleSyncRepeatingTask((Plugin) this, new Runnable() {

				@Override
				public void run() {
					PacketContainer container = new PacketContainer(PacketType.Play.Server.PLAYER_LIST_HEADER_FOOTER);
					container.getChatComponents().write(0,
							WrappedChatComponent.fromText((String) config.getTabListHeader()));
					container.getChatComponents().write(1,
							WrappedChatComponent.fromText((String) config.getTabListFooter()));
					Breakpoint.this.prm.broadcastServerPacket(container);
				}
			}, 0L, 20L);
			return;
		}
		this.successfullyEnabled = false;
		ConsoleCommandSender sender = Bukkit.getConsoleSender();
		sender.sendMessage((Object) ChatColor.RED + "  #-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#");
		sender.sendMessage((Object) ChatColor.RED + " # Nen\u00ed licence na spu\u0161t\u011bn\u00ed Breakpointu #");
		sender.sendMessage((Object) ChatColor.RED + "#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#-#");
		this.getServer().getPluginManager().registerEvents((Listener) new BanListener(), (Plugin) this);
	}

	public void reload() {
		if (!this.successfullyEnabled) {
			return;
		}
		this.trySave();
		if (this.evtm != null) {
			this.evtm.save();
		}
		this.getServer().getScheduler().cancelTasks((Plugin) this);
		instance = null;
		config = null;
		this.onEnable();
	}

	public void onDisable() {
		if (!this.successfullyEnabled) {
			return;
		}
		this.trySave();
		this.kickPlayers();
		if (this.evtm != null) {
			this.evtm.save();
		}
		this.getServer().getScheduler().cancelTasks((Plugin) this);
		instance = null;
		config = null;
	}

	public void save() throws IOException {
		BPPlayer.saveOnlinePlayersData();
		Clan.saveClans();
		config.save();
		GameManager.saveGames();
	}

	public void trySave() {
		try {
			this.save();
		} catch (IOException e) {
			Breakpoint.warn("Error when saving Breakpoint data: " + e.getMessage());
			e.printStackTrace();
		}
	}

	public void redirectCommands() {
		Server server = this.getServer();
		server.getPluginCommand("bp").setExecutor(new BPCommandExecutor(this));
		server.getPluginCommand("helpop").setExecutor(new HelpOPCommandExecutor());
		server.getPluginCommand("clan").setExecutor(new ClanCommandExecutor());
		server.getPluginCommand("achievements").setExecutor((CommandExecutor) new AchievementsCommandExecutor());
		server.getPluginCommand("top").setExecutor((CommandExecutor) new TopCommandExecutor());
		server.getPluginCommand("topclans").setExecutor((CommandExecutor) new TopClansCommandExecutor());
		server.getPluginCommand("rank").setExecutor((CommandExecutor) new RankCommandExecutor());
		server.getPluginCommand("gm").setExecutor((CommandExecutor) new GMCommandExecutor());
		server.getPluginCommand("skull").setExecutor((CommandExecutor) new SkullCommandExecutor());
		server.getPluginCommand("fly").setExecutor((CommandExecutor) new FlyCommandExecutor());
		server.getPluginCommand("cw").setExecutor((CommandExecutor) new CWCommandExecutor());
		server.getPluginCommand("shop").setExecutor((CommandExecutor) new ShopCommand());
	}

	public void registerListeners() {
		PluginManager pm = this.getServer().getPluginManager();
		pm.registerEvents((Listener) new PlayerInteractListener(this), (Plugin) this);
		pm.registerEvents((Listener) new PlayerConnectionListener(this), (Plugin) this);
		pm.registerEvents((Listener) new PVPListener(this), (Plugin) this);
		pm.registerEvents((Listener) new ChatListener(), (Plugin) this);
		pm.registerEvents((Listener) new PlayerInventoryListener(this), (Plugin) this);
		this.prm.addPacketListener((PacketListener) new PacketAdapter((Plugin) this,
				new PacketType[] { PacketType.Play.Server.ENTITY_EQUIPMENT }) {

			public void onPacketSending(PacketEvent event) {
				ItemStack stack;
				Player player = event.getPlayer();
				if (player == null) {
					return;
				}
				World world = player.getWorld();
				PacketContainer packet = event.getPacket();
				Entity entity = (Entity) packet.getEntityModifier(world).read(0);
				if (entity instanceof Player) {
					Player viewed = (Player) entity;
					String viewedName = viewed.getName();
					String playerName = player.getName();
					Clan viewedClan = Clan.getByPlayer(viewedName);
					Clan playerClan = Clan.getByPlayer(playerName);
					if (viewedClan != null && playerClan != null && viewedClan.equals(playerClan)) {
						return;
					}
				}
				if ((stack = (ItemStack) packet.getItemModifier().read(0)) != null) {
					Set<Enchantment> encs = stack.getEnchantments().keySet();
					for (Enchantment enc : encs) {
						stack.removeEnchantment(enc);
					}
				}
			}
		});
		this.prm.addPacketListener((PacketListener) new PacketAdapter((Plugin) this,
				new PacketType[] { PacketType.Play.Server.WINDOW_ITEMS }) {

			@SuppressWarnings("unchecked")
			public void onPacketSending(PacketEvent event) {
				Player player = event.getPlayer();
				BPPlayer bpPlayer = BPPlayer.get(player);
				if (bpPlayer == null || !bpPlayer.isInGame()) {
					return;
				}
				Settings settings = bpPlayer.getSettings();
				if (settings.hasShowEnchantments()) {
					return;
				}
				PacketContainer packet = event.getPacket();
				List<ItemStack> stacks = (List) packet.getItemListModifier().read(0);
				if (stacks != null) {
					for (ItemStack stack : stacks) {
						if (stack == null)
							continue;
						Breakpoint.removeEnchantments(stack);
					}
				}
			}
		});
		this.prm.addPacketListener((PacketListener) new PacketAdapter((Plugin) this,
				new PacketType[] { PacketType.Play.Server.SET_SLOT }) {

			public void onPacketSending(PacketEvent event) {
				Player player = event.getPlayer();
				BPPlayer bpPlayer = BPPlayer.get(player);
				if (bpPlayer == null || !bpPlayer.isInGame()) {
					return;
				}
				Settings settings = bpPlayer.getSettings();
				if (settings.hasShowEnchantments()) {
					return;
				}
				PacketContainer packet = event.getPacket();
				ItemStack stack = (ItemStack) packet.getItemModifier().read(0);
				if (stack != null) {
					Breakpoint.removeEnchantments(stack);
				}
			}
		});
	}

	private static void removeEnchantments(ItemStack stack) {
		Map<Enchantment, Integer> entries = stack.getEnchantments();
		if (entries == null || entries.size() <= 0) {
			return;
		}
		ItemMeta im = stack.getItemMeta();
		List<String> lore = im.hasLore() ? im.getLore() : new LinkedList<String>();
		for (Entry<Enchantment, Integer> entry : entries.entrySet()) {
			Enchantment type = (Enchantment) entry.getKey();
			Integer level = (Integer) entry.getValue();
			im.removeEnchant(type);
			lore.add(ChatColor.GRAY + type.getName() + " " + level);
		}
		im.setLore(lore);
		stack.setItemMeta(im);
	}

	public static void info(String string) {
		Bukkit.getConsoleSender().sendMessage("[Breakpoint] " + string);
	}

	public static void warn(String string) {
		Bukkit.getConsoleSender().sendMessage("[Breakpoint] [Warning] " + string);
		for (Player player : Bukkit.getOnlinePlayers()) {
			if (!player.hasPermission("Breakpoint.receiveWarnings"))
				continue;
			player.sendMessage(MessageType.CHAT_BREAKPOINT.getTranslation().getValue(new Object[0])
					+ (Object) ChatColor.RED + " [Warning] " + string);
		}
	}

	public static void broadcast(String string, boolean prefix) {
		for (Player player : Bukkit.getOnlinePlayers()) {
			player.sendMessage(
					(prefix ? MessageType.CHAT_BREAKPOINT.getTranslation().getValue(new Object[0]) : ChatColor.YELLOW)
							+ " " + string);
		}
		Bukkit.getConsoleSender().sendMessage("[Breakpoint] [Broadcast] " + string);
	}

	public static void broadcast(String string) {
		Breakpoint.broadcast(string, false);
	}

	public static void clearChat() {
		for (Player player : Bukkit.getOnlinePlayers()) {
			for (int i = 0; i < 10; ++i) {
				player.sendMessage("");
			}
		}
	}

	public void clearChat(Player player) {
		for (int i = 0; i < 10; ++i) {
			player.sendMessage("");
		}
	}

	public void kickPlayers() {
		String msg = MessageType.CHAT_BREAKPOINT.getTranslation().getValue(new Object[0]) + " "
				+ MessageType.OTHER_RESTART.getTranslation().getValue(new Object[0]);
		for (Player player : Bukkit.getOnlinePlayers()) {
			player.kickPlayer(msg);
		}
	}

	public void setEventManager() {
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(1);
		int month = calendar.get(2);
		int dayOfMonth = calendar.get(5);
		if (month == 11 && dayOfMonth <= 24) {
			this.evtm = AdventManager.load(year);
		}
	}

	public void setEventManager(EventManager e) {
		this.evtm = e;
	}

	public boolean hasEvent() {
		return this.evtm != null;
	}

	public EventManager getEventManager() {
		return this.evtm;
	}

	public MapManager getMapManager() {
		return this.mapm;
	}

	public static Breakpoint getInstance() {
		return instance;
	}

	public static Configuration getBreakpointConfig() {
		return config;
	}

	public static FruitSQL getMySQL() {
		return mySQL;
	}

	public static boolean hasMySQL() {
		return mySQL != null;
	}

	public static BreakpointCommand getExcternalBPCommandExecutor() {
		return externalExceturorsHandler;
	}

	public NametagAPIHooks getNametagAPIHook() {
		return this.nameTagAPIHook;
	}

	public static String getVersion() {
		return "@VERSION@";
	}

}
